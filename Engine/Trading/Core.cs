﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using RocketCore;

using System.Diagnostics;

namespace RocketCore
{
    [Serializable]
    class Core
    {
        Stopwatch sw = new Stopwatch();

        #region CORE VARIABLES
        
        #region DATA COLLECTIONS

        private Dictionary<int, Account> Accounts; //торговые счета юзеров
        private List<Order> ActiveBuyOrders; //активные заявки на покупку
        private List<Order> ActiveSellOrders; //активные заявки на продажу
        private List<Order> LongSLs; //стоп-лоссы для лонгов
        private List<Order> ShortSLs; //стоп-лоссы для шортов
        private List<Order> LongTPs; //тейк-профиты для лонгов              
        private List<Order> ShortTPs; //тейк-профиты для шортов 
        private List<TSOrder> LongTSs; //трейлинг-стопы для лонгов
        private List<TSOrder> ShortTSs; //трейлинг-стопы для шортов
        private Dictionary<string, FixAccount> FixAccounts; //FIX-счета
        private Dictionary<string, ApiKey> ApiKeys; //API-ключи

        #endregion

        #region MARGIN PARAMETERS

        private decimal max_leverage; 
        private decimal mc_level; 
        private decimal fl_level; 

        #endregion

        #region BUFFER VARIABLES

        private decimal bid_buf;
        private decimal ask_buf;
        private int act_buy_buf_max_size; //TODO change size on-the-fly
        private int act_sell_buf_max_size; //TODO change size on-the-fly
        private List<OrderBuf> act_buy_buf;
        private List<OrderBuf> act_sell_buf;
        
        #endregion

        #endregion

        #region CORE CONSTRUCTORS

        internal Core()
        {
            Accounts = new Dictionary<int, Account>(3000);
            ActiveBuyOrders = new List<Order>(5000);
            ActiveSellOrders = new List<Order>(5000);
            LongSLs = new List<Order>(2000);
            ShortSLs = new List<Order>(2000);
            LongTPs = new List<Order>(2000);
            ShortTPs = new List<Order>(2000);
            LongTSs = new List<TSOrder>(1000);
            ShortTSs = new List<TSOrder>(1000);
            FixAccounts = new Dictionary<string, FixAccount>(500);
            ApiKeys = new Dictionary<string, ApiKey>(500);

            max_leverage = 5m; //максимальное плечо 1:5
            mc_level = 0.14m; //уровень Margin Call 14%
            fl_level = 0.07m; //уровень Forced Liquidation 7%

            bid_buf = 0m;
            ask_buf = 0m;
            act_buy_buf_max_size = 30;
            act_sell_buf_max_size = 30;
            act_buy_buf = new List<OrderBuf>(act_buy_buf_max_size);
            act_sell_buf = new List<OrderBuf>(act_sell_buf_max_size);
        }

        #endregion

        #region CALLABLE CORE FUNCTIONS

        #region NATIVE FUNCTIONS

        #region USER FUNCTIONS

        internal StatusCodes CreateAccount(int user_id) //открыть торговый счёт
        {
            //Pusher.ReplicateFC((int)FuncIds.CreateAccount, new string[] { user_id.ToString() }); //репликация вызова функции

            if (!Accounts.ContainsKey(user_id)) //если счёт ещё не открыт, то открываем
            {
                Accounts.Add(user_id, new Account());
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorAccountAlreadyExists;
        }

        internal StatusCodes SuspendAccount(int user_id) //заблокировать торговый счёт
        {
            //Pusher.ReplicateFC((int)FuncIds.SuspendAccount, new string[] { user_id.ToString() }); //репликация вызова функции

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то блокируем
            {
                if (!acc.Suspended)
                {
                    acc.Suspended = true;
                    return StatusCodes.Success;
                }
                else return StatusCodes.ErrorAccountAlreadySuspended;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes UnsuspendAccount(int user_id) //разблокировать торговый счёт
        {
            //Pusher.ReplicateFC((int)FuncIds.UnsuspendAccount, new string[] { user_id.ToString() }); //репликация вызова функции

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то разблокируем
            {
                if (acc.Suspended)
                {
                    acc.Suspended = false;
                    return StatusCodes.Success;
                }
                else return StatusCodes.ErrorAccountAlreadyUnsuspended;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes DeleteAccount(int user_id) //удалить торговый счёт
        {
            //Pusher.ReplicateFC((int)FuncIds.DeleteAccount, new string[] { user_id.ToString() }); //репликация вызова функции

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то удаляем
            {
                //удаляем все зависимости
                ActiveBuyOrders.RemoveAll(i => i.UserId == user_id);
                ActiveSellOrders.RemoveAll(i => i.UserId == user_id);
                LongSLs.RemoveAll(i => i.UserId == user_id);
                ShortSLs.RemoveAll(i => i.UserId == user_id);
                LongTPs.RemoveAll(i => i.UserId == user_id);
                ShortTPs.RemoveAll(i => i.UserId == user_id);
                LongTSs.RemoveAll(i => i.UserId == user_id);
                ShortTSs.RemoveAll(i => i.UserId == user_id);                
                RemoveUserFixAccounts(user_id);
                RemoveUserApiKeys(user_id);

                //удаляем юзера
                Accounts.Remove(user_id);
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes DepositFunds(int user_id, bool currency, decimal sum) //пополнить торговый счёт
        {
            //Pusher.ReplicateFC((int)FuncIds.DepositFunds, new string[] { user_id.ToString(), currency.ToInt32().ToString(), sum.ToString() }); //репликация вызова функции

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то пополняем
            {
                if (sum > 0) //проверка на положительность суммы пополнения
                {
                    if (!currency) //0 - первая валюта пары
                    {
                        acc.AvailableFunds1 += sum;
                        Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                        return StatusCodes.Success;
                    }
                    else //1 - вторая валюта пары
                    {
                        acc.AvailableFunds2 += sum;
                        Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                        return StatusCodes.Success;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroSum;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes WithdrawFunds(int user_id, bool currency, decimal sum) //снять с торгового счёта
        {
            //Pusher.ReplicateFC((int)FuncIds.WithdrawFunds, new string[] { user_id.ToString(), currency.ToInt32().ToString(), sum.ToString() }); //репликация вызова функции

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то снимаем
            {
                if (acc.Suspended) return StatusCodes.ErrorAccountSuspended; //проверка на блокировку счёта
                if (acc.AvailableFunds1 < 0 || acc.AvailableFunds2 < 0) return StatusCodes.ErrorBorrowedFundsUse; //проверка на использование заёмных средств

                if (sum > 0) //проверка на положительность суммы вывода
                {
                    if (!currency) //0 - первая валюта пары
                    {
                        if (acc.AvailableFunds1 >= sum) //проверка на наличие средств для снятия
                        {
                            acc.AvailableFunds1 -= sum;
                            Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorInsufficientFunds;
                    }
                    else  //1 - вторая валюта пары
                    {
                        if (acc.AvailableFunds2 >= sum) //проверка на наличие средств для снятия
                        {
                            acc.AvailableFunds2 -= sum;
                            Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorInsufficientFunds;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroSum;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes BaseLimit(int user_id, bool order_kind, decimal amount, decimal rate, int msg_type, long func_call_id, int fc_source, out Order order, string external_data) //базовый метод размещения заявки 
        {
            //Pusher.ReplicateFC((int)FuncIds.BaseLimit, new string[] { user_id.ToString(), order_kind.ToInt32().ToString(), amount.ToString(), rate.ToString() }); //репликация вызова функции

            //инициализация
            order = new Order();

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то снимаем с него сумму и подаём заявку
            {
                if (acc.Suspended) return StatusCodes.ErrorAccountSuspended; //проверка на блокировку счёта
                if (amount <= 0 || rate <= 0) return StatusCodes.ErrorNegativeOrZeroSum; //проверка на положительность rate и amount 

                if (!order_kind) //если заявка на покупку (0)
                {
                    if (acc.AvailableFunds2 >= amount * rate) //проверка на платежеспособность по currency2
                    {
                        acc.AvailableFunds2 -= amount * rate; //снимаем средства с доступных средств
                        acc.BlockedFunds2 += amount * rate; //блокируем средства в заявке на покупку
                        Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                        order = new Order(user_id, amount, amount, rate, fc_source, external_data);
                        BSInsertion.AddBuyOrder(ref ActiveBuyOrders, order);
                        Pusher.NewOrder(msg_type, func_call_id, fc_source, order_kind, order); //сообщение о новой заявке
                        //FixMessager.NewMarketDataIncrementalRefresh(order_kind, order); //FIX multicast
                        if (fc_source == (int)FCSources.FixApi) FixMessager.NewExecutionReport(external_data, func_call_id, order_kind, order); //FIX-сообщение о новой заявке
                        Match();
                        return StatusCodes.Success;
                    }
                    else //лонг по лимиту
                    {
                        //калькуляция рыночного курса на покупку (лонг будет распродаваться) 
                        decimal long_market_rate = 0m;
                        decimal long_accumulated_amount = 0m;
                        for (int i = ActiveBuyOrders.Count - 1; i >= 0; i--)
                        {
                            long_accumulated_amount += ActiveBuyOrders[i].ActualAmount;
                            if (long_accumulated_amount >= amount) //если объём накопленных заявок на продажу покрывает объём заявки на покупку
                            {
                                long_market_rate = ActiveBuyOrders[i].Rate;
                                break;
                            }
                        }

                        if (long_market_rate == 0) return StatusCodes.ErrorInsufficientMarketVolume;

                        if (acc.AvailableFunds2 >= 0) //юзер ещё не в лонге
                        {
                            if ((acc.AvailableFunds1 * long_market_rate + acc.AvailableFunds2) * max_leverage >= amount * long_market_rate) //условие размещения лонга
                            {
                                acc.AvailableFunds2 -= amount * rate; //снимаем средства с доступных средств
                                acc.BlockedFunds2 += amount * rate; //блокируем средства в заявке на покупку
                                Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                                order = new Order(user_id, amount, amount, rate, fc_source, external_data);
                                BSInsertion.AddBuyOrder(ref ActiveBuyOrders, order);
                                Pusher.NewOrder(msg_type, func_call_id, fc_source, order_kind, order); //сообщение о новой заявке
                                //FixMessager.NewMarketDataIncrementalRefresh(order_kind, order); //FIX multicast
                                if (fc_source == (int)FCSources.FixApi) FixMessager.NewExecutionReport(external_data, func_call_id, order_kind, order); //FIX-сообщение о новой заявке
                                Match();
                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorInsufficientFunds;
                        }
                        else //юзер уже в лонге
                        {
                            if ((acc.AvailableFunds1 * long_market_rate + acc.AvailableFunds2) * max_leverage >= amount * long_market_rate + acc.AvailableFunds2 * (-1m)) //условие размещения лонга
                            {
                                acc.AvailableFunds2 -= amount * rate; //снимаем средства с доступных средств
                                acc.BlockedFunds2 += amount * rate; //блокируем средства в заявке на покупку
                                Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                                order = new Order(user_id, amount, amount, rate, fc_source, external_data);
                                BSInsertion.AddBuyOrder(ref ActiveBuyOrders, order);
                                Pusher.NewOrder(msg_type, func_call_id, fc_source, order_kind, order); //сообщение о новой заявке
                                //FixMessager.NewMarketDataIncrementalRefresh(order_kind, order); //FIX multicast
                                if (fc_source == (int)FCSources.FixApi) FixMessager.NewExecutionReport(external_data, func_call_id, order_kind, order); //FIX-сообщение о новой заявке
                                Match();
                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorInsufficientFunds; 
                        }
                    }
                }
                else //если заявка на продажу (1)
                {
                    if (acc.AvailableFunds1 >= amount) //проверка на платежеспособность по currency1
                    {
                        acc.AvailableFunds1 -= amount; //снимаем средства с доступных средств
                        acc.BlockedFunds1 += amount; //блокируем средства в заявке на продажу
                        Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                        order = new Order(user_id, amount, amount, rate, fc_source, external_data);
                        BSInsertion.AddSellOrder(ref ActiveSellOrders, order);
                        Pusher.NewOrder(msg_type, func_call_id, fc_source, order_kind, order); //сообщение о новой заявке
                        //FixMessager.NewMarketDataIncrementalRefresh(order_kind, order); //FIX multicast
                        if (fc_source == (int)FCSources.FixApi) FixMessager.NewExecutionReport(external_data, func_call_id, order_kind, order); //FIX-сообщение о новой заявке
                        Match();
                        return StatusCodes.Success;
                    }
                    else //шорт по лимиту
                    {
                        //калькуляция рыночного курса на продажу (шорт будет выкупаться) 
                        decimal short_market_rate = 0m;
                        decimal short_accumulated_amount = 0m;
                        for (int i = ActiveSellOrders.Count - 1; i >= 0; i--)
                        {
                            short_accumulated_amount += ActiveSellOrders[i].ActualAmount;
                            if (short_accumulated_amount >= amount) //если объём накопленных заявок на продажу покрывает объём заявки на покупку
                            {
                                short_market_rate = ActiveSellOrders[i].Rate;
                                break;
                            }
                        }

                        if (short_market_rate == 0) return StatusCodes.ErrorInsufficientMarketVolume;

                        if (acc.AvailableFunds1 >= 0) //юзер ещё не в шорте
                        {
                            if ((acc.AvailableFunds1 * short_market_rate + acc.AvailableFunds2) * max_leverage >= amount * short_market_rate) //условие размещения шорта
                            {
                                acc.AvailableFunds1 -= amount; //снимаем средства с доступных средств
                                acc.BlockedFunds1 += amount; //блокируем средства в заявке на продажу
                                Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                                order = new Order(user_id, amount, amount, rate, fc_source, external_data);
                                BSInsertion.AddSellOrder(ref ActiveSellOrders, order);
                                Pusher.NewOrder(msg_type, func_call_id, fc_source, order_kind, order); //сообщение о новой заявке
                                //FixMessager.NewMarketDataIncrementalRefresh(order_kind, order); //FIX multicast
                                if (fc_source == (int)FCSources.FixApi) FixMessager.NewExecutionReport(external_data, func_call_id, order_kind, order); //FIX-сообщение о новой заявке
                                Match();
                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorInsufficientFunds;
                        }
                        else //юзер уже в шорте
                        {
                            if ((acc.AvailableFunds1 * short_market_rate + acc.AvailableFunds2) * max_leverage >= (amount + acc.AvailableFunds1 * (-1m)) * short_market_rate) //условие размещения шорта
                            {
                                acc.AvailableFunds1 -= amount; //снимаем средства с доступных средств
                                acc.BlockedFunds1 += amount; //блокируем средства в заявке на продажу
                                Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе
                                order = new Order(user_id, amount, amount, rate, fc_source, external_data);
                                BSInsertion.AddSellOrder(ref ActiveSellOrders, order);
                                Pusher.NewOrder(msg_type, func_call_id, fc_source, order_kind, order); //сообщение о новой заявке
                                //FixMessager.NewMarketDataIncrementalRefresh(order_kind, order); //FIX multicast
                                if (fc_source == (int)FCSources.FixApi) FixMessager.NewExecutionReport(external_data, func_call_id, order_kind, order); //FIX-сообщение о новой заявке
                                Match();
                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorInsufficientFunds;
                        }
                    }
                }
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes PlaceLimit(int user_id, bool order_kind, decimal amount, decimal rate, long func_call_id, int fc_source, out Order order, string external_data = "") //подать лимитную заявку
        {
            return BaseLimit(user_id, order_kind, amount, rate, (int)MessageTypes.NewPlaceLimit, func_call_id, fc_source, out order, external_data);
        }

        internal StatusCodes PlaceMarket(int user_id, bool order_kind, decimal amount, long func_call_id, int fc_source, out Order order, string external_data = "") //подать рыночную заявку
        {
            if (!order_kind) //если заявка на покупку (0)
            {
                //калькуляция rate для немедленного исполнения по рынку
                decimal market_rate = 0m;
                decimal accumulated_amount = 0m;
                for (int i = ActiveSellOrders.Count - 1; i >= 0; i--)
                {
                    accumulated_amount += ActiveSellOrders[i].ActualAmount;
                    if (accumulated_amount >= amount) //если объём накопленных заявок на продажу покрывает объём заявки на покупку
                    {
                        market_rate = ActiveSellOrders[i].Rate;
                        break;
                    }
                }

                if (market_rate == 0)
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorInsufficientMarketVolume;
                }

                return BaseLimit(user_id, order_kind, amount, market_rate, (int)MessageTypes.NewPlaceMarket, func_call_id, fc_source, out order, external_data);
            }
            else //если заявка на продажу (1)
            {
                //калькуляция rate для немедленного исполнения по рынку
                decimal market_rate = 0m;
                decimal accumulated_amount = 0m;
                for (int i = ActiveBuyOrders.Count - 1; i >= 0; i--)
                {
                    accumulated_amount += ActiveBuyOrders[i].ActualAmount;
                    if (accumulated_amount >= amount) //если объём накопленных заявок на покупку покрывает объём заявки на продажу
                    {
                        market_rate = ActiveBuyOrders[i].Rate;
                        break;
                    }
                }

                if (market_rate == 0)
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorInsufficientMarketVolume;
                }

                return BaseLimit(user_id, order_kind, amount, market_rate, (int)MessageTypes.NewPlaceMarket, func_call_id, fc_source, out order, external_data);
            }
        }

        internal StatusCodes PlaceInstant(int user_id, bool order_kind, decimal total, long func_call_id, int fc_source, out Order order, string external_data = "") //подать рыночную заявку (на входе валюта 2)
        {
            if (!order_kind) //если заявка на покупку (0)
            {
                //калькуляция rate для немедленного исполнения по рынку
                decimal market_rate = 0m;
                decimal accumulated_amount = 0m; //в валюте 1
                decimal accumulated_total = 0m; //в валюте 2
                for (int i = ActiveSellOrders.Count - 1; i >= 0; i--)
                {
                    Order sell_ord = ActiveSellOrders[i];
                    accumulated_total += sell_ord.ActualAmount * sell_ord.Rate;
                    if (accumulated_total >= total) //если накопленная сумма в заявках на продажу превышает сумму в заявке на покупку
                    {
                        accumulated_amount += (total - accumulated_total + sell_ord.ActualAmount * sell_ord.Rate) / sell_ord.Rate;
                        market_rate = sell_ord.Rate;
                        break;
                    }
                    else //если ещё не превышает - учитываем кол-во валюты 1
                    {
                        accumulated_amount += sell_ord.ActualAmount;
                    }
                }

                if (market_rate == 0)
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorInsufficientMarketVolume;
                }

                return BaseLimit(user_id, order_kind, accumulated_amount, market_rate, (int)MessageTypes.NewPlaceInstant, func_call_id, fc_source, out order, external_data);
            }
            else //если заявка на продажу (1)
            {
                //калькуляция rate для немедленного исполнения по рынку
                decimal market_rate = 0m;
                decimal accumulated_amount = 0m; //в валюте 1
                decimal accumulated_total = 0m; //в валюте 2
                for (int i = ActiveBuyOrders.Count - 1; i >= 0; i--)
                {
                    Order buy_ord = ActiveBuyOrders[i];
                    accumulated_total += buy_ord.ActualAmount * buy_ord.Rate;
                    if (accumulated_total >= total) //если накопленная сумма в заявках на покупку превышает сумму в заявке на продажу
                    {
                        accumulated_amount += (total - accumulated_total + buy_ord.ActualAmount * buy_ord.Rate) / buy_ord.Rate;
                        market_rate = buy_ord.Rate;
                        break;
                    }
                    else //если ещё не превышает - учитываем кол-во валюты 1
                    {
                        accumulated_amount += buy_ord.ActualAmount;
                    }
                }

                if (market_rate == 0)
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorInsufficientMarketVolume;
                }

                return BaseLimit(user_id, order_kind, accumulated_amount, market_rate, (int)MessageTypes.NewPlaceInstant, func_call_id, fc_source, out order, external_data);
            }
        }

        internal StatusCodes CancelOrder(int user_id, long order_id, long func_call_id, int fc_source, out bool order_kind, out Order order) //отменить заявку
        {
            //Pusher.ReplicateFC((int)FuncIds.CancelOrder, new string[] { user_id.ToString(), order_id.ToString() }); //репликация вызова функции

            //инициализация
            order_kind = false;
            order = new Order();

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то отменяем заявку
            {
                if (order_id > 0) //проверка на положительность ID заявки
                {
                    int buy_index = ActiveBuyOrders.FindIndex(i => i.OrderId == order_id);
                    if (buy_index >= 0) //заявка найдена в ActiveBuyOrders
                    {
                        order = ActiveBuyOrders[buy_index];
                        if (order.UserId == user_id) //данная заявка принадлежит данному юзеру
                        {
                            order_kind = false;

                            ActiveBuyOrders.RemoveAt(buy_index);                            
                            Pusher.NewOrder((int)MessageTypes.NewCancelOrder, func_call_id, fc_source, order_kind, order); //сообщение о новой отмене заявки
                            acc.BlockedFunds2 -= order.ActualAmount * order.Rate;
                            acc.AvailableFunds2 += order.ActualAmount * order.Rate;
                            Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе

                            if (UpdTicker()) Pusher.NewTicker(bid_buf, ask_buf, DateTime.Now); //сообщение о новом тикере
                            if (UpdActiveBuyTop()) Pusher.NewActiveBuyTop(act_buy_buf, DateTime.Now); //сообщение о новом топе стакана на покупку
                            if (UpdActiveSellTop()) Pusher.NewActiveSellTop(act_sell_buf, DateTime.Now); //сообщение о новом топе стакана на продажу

                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorCrossUserAccessDenied;
                    }
                    else
                    {
                        int sell_index = ActiveSellOrders.FindIndex(i => i.OrderId == order_id);
                        if (sell_index >= 0) //заявка найдена в ActiveSellOrders
                        {
                            order = ActiveSellOrders[sell_index];
                            if (order.UserId == user_id) //данная заявка принадлежит данному юзеру
                            {
                                order_kind = true;

                                ActiveSellOrders.RemoveAt(sell_index);
                                Pusher.NewOrder((int)MessageTypes.NewCancelOrder, func_call_id, fc_source, order_kind, order); //сообщение о новой отмене заявки
                                acc.BlockedFunds1 -= order.ActualAmount;
                                acc.AvailableFunds1 += order.ActualAmount;
                                Pusher.NewBalance(user_id, acc, DateTime.Now); //сообщение о новом балансе

                                if (UpdTicker()) Pusher.NewTicker(bid_buf, ask_buf, DateTime.Now); //сообщение о новом тикере
                                if (UpdActiveBuyTop()) Pusher.NewActiveBuyTop(act_buy_buf, DateTime.Now); //сообщение о новом топе стакана на покупку
                                if (UpdActiveSellTop()) Pusher.NewActiveSellTop(act_sell_buf, DateTime.Now); //сообщение о новом топе стакана на продажу

                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorCrossUserAccessDenied;
                        }
                        else return StatusCodes.ErrorOrderNotFound;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroId;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes AddSL(int user_id, bool position_type, decimal amount, decimal rate, long func_call_id, int fc_source, out Order order, string external_data = "") //добавить стоп-лосс
        {
            //Pusher.ReplicateFC((int)FuncIds.AddSL, new string[] { user_id.ToString(), position_type.ToInt32().ToString(), amount.ToString(), rate.ToString() }); //репликация вызова функции

            //инициализация
            order = new Order();

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то добавляем SL
            {
                if (amount <= 0 || rate <= 0) return StatusCodes.ErrorNegativeOrZeroSum; //проверка на положительность rate и amount

                if (!position_type) //стоп-лосс для лонга (0) => будет sell-заявка
                {
                    if (acc.AvailableFunds1 < 0) return StatusCodes.ErrorIncorrectPositionType; //проверка соответствия SL позиции юзера

                    //проверка уровня SL по отношению к рыночной цене на покупку
                    if (ActiveBuyOrders.Count > 0)
                    {
                        if (ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate > rate) //сравнение с рыночным курсом на покупку (стоп-лосс будет на продажу)
                        {
                            order = new Order(user_id, amount, amount, rate, fc_source, external_data);
                            BSInsertion.AddBuyOrder(ref LongSLs, order); //сортировка DESC
                            Pusher.NewOrder((int)MessageTypes.NewAddSL, func_call_id, fc_source, position_type, order); //сообщение о новом SL
                            if (fc_source == (int)FCSources.FixApi) FixMessager.NewExecutionReport(external_data, func_call_id, position_type, order); //FIX-сообщение о новом SL
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorIncorrectRate;
                    }
                    else return StatusCodes.ErrorInsufficientMarketVolume;
                }
                else //стоп-лосс для шорта (1) => будет buy-заявка
                {
                    if (acc.AvailableFunds2 < 0) return StatusCodes.ErrorIncorrectPositionType; //проверка соответствия SL позиции юзера

                    //проверка уровня SL по отношению к рыночной цене на продажу
                    if (ActiveSellOrders.Count > 0)
                    {
                        if (ActiveSellOrders[ActiveSellOrders.Count - 1].Rate < rate) //сравнение с рыночным курсом на продажу (стоп-лосс будет на покупку)
                        {
                            order = new Order(user_id, amount, amount, rate, fc_source, external_data);
                            BSInsertion.AddSellOrder(ref ShortSLs, order); //сортировка ASC
                            Pusher.NewOrder((int)MessageTypes.NewAddSL, func_call_id, fc_source, position_type, order); //сообщение о новом SL
                            if (fc_source == (int)FCSources.FixApi) FixMessager.NewExecutionReport(external_data, func_call_id, position_type, order); //FIX-сообщение о новом SL
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorIncorrectRate;
                    }
                    else return StatusCodes.ErrorInsufficientMarketVolume;
                }
            }
            else return StatusCodes.ErrorAccountNotFound;
        }
            
        internal StatusCodes AddTP(int user_id, bool position_type, decimal amount, decimal rate, long func_call_id, int fc_source, out Order order) //добавить тейк-профит
        {
            //Pusher.ReplicateFC((int)FuncIds.AddTP, new string[] { user_id.ToString(), position_type.ToInt32().ToString(), amount.ToString(), rate.ToString() }); //репликация вызова функции

            //инициализация
            order = new Order();

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то добавляем TP
            {
                if (amount <= 0 || rate <= 0) return StatusCodes.ErrorNegativeOrZeroSum; //проверка на положительность rate и amount

                if (!position_type) //тейк-профит для лонга (0) => будет sell-заявка
                {
                    if (acc.AvailableFunds1 < 0) return StatusCodes.ErrorIncorrectPositionType; //проверка соответствия TP позиции юзера

                    //проверка уровня TP по отношению к рыночной цене на покупку
                    if (ActiveBuyOrders.Count > 0)
                    {
                        if (ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate < rate) //сравнение с рыночным курсом на покупку (тейк-профит будет на продажу)
                        {
                            order = new Order(user_id, amount, amount, rate);
                            BSInsertion.AddSellOrder(ref LongTPs, order); //сортировка ASC
                            Pusher.NewOrder((int)MessageTypes.NewAddTP, func_call_id, fc_source, position_type, order); //сообщение о новом TP
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorIncorrectRate;
                    }
                    else return StatusCodes.ErrorInsufficientMarketVolume;
                }
                else //тейк-профит для шорта (1) => будет buy-заявка
                {
                    if (acc.AvailableFunds2 < 0) return StatusCodes.ErrorIncorrectPositionType; //проверка соответствия TP позиции юзера
                    
                    //проверка уровня TP по отношению к рыночной цене на продажу
                    if (ActiveSellOrders.Count > 0)
                    {
                        if (ActiveSellOrders[ActiveSellOrders.Count - 1].Rate > rate) //сравнение с рыночным курсом на продажу (тейк-профит будет на покупку)
                        {
                            order = new Order(user_id, amount, amount, rate);
                            BSInsertion.AddBuyOrder(ref ShortTPs, order); //сортировка DESC
                            Pusher.NewOrder((int)MessageTypes.NewAddTP, func_call_id, fc_source, position_type, order); //сообщение о новом TP
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorIncorrectRate;
                    }
                    else return StatusCodes.ErrorInsufficientMarketVolume;
                }
            }
            else return StatusCodes.ErrorAccountNotFound; 
        }

        internal StatusCodes AddTS(int user_id, bool position_type, decimal amount, decimal offset, long func_call_id, int fc_source, out TSOrder ts_order) //добавить трейлинг-стоп
        {
            //Pusher.ReplicateFC((int)FuncIds.AddTS, new string[] { user_id.ToString(), position_type.ToInt32().ToString(), amount.ToString(), offset.ToString() }); //репликация вызова функции

            //инициализация
            ts_order = new TSOrder();

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то добавляем SL
            {
                if (amount <= 0 || offset <= 0) return StatusCodes.ErrorNegativeOrZeroSum; //проверка на положительность rate и offset

                if (!position_type) //трейлинг-стоп для лонга (0) => будет sell-заявка
                {
                    if (acc.AvailableFunds1 < 0) return StatusCodes.ErrorIncorrectPositionType; //проверка соответствия TS позиции юзера

                    //проверка уровня TS по отношению к рыночной цене на покупку
                    if (ActiveBuyOrders.Count > 0)
                    {
                        decimal init_rate = ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate - offset;
                        if (init_rate > 0) //проверка положительности рейта TS
                        {
                            ts_order = new TSOrder(user_id, amount, amount, init_rate, offset);
                            LongTSs.Add(ts_order); //сортировка не нужна
                            Pusher.NewOrder((int)MessageTypes.NewAddTS, func_call_id, fc_source, position_type, ts_order); //сообщение о новом TS
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorIncorrectRate;
                    }
                    else return StatusCodes.ErrorInsufficientMarketVolume;
                }
                else //трейлинг-стоп для шорта (1) => будет buy-заявка
                {
                    if (acc.AvailableFunds2 < 0) return StatusCodes.ErrorIncorrectPositionType; //проверка соответствия TS позиции юзера

                    //проверка уровня TS по отношению к рыночной цене на продажу
                    if (ActiveSellOrders.Count > 0)
                    {
                        decimal init_rate = ActiveSellOrders[ActiveSellOrders.Count - 1].Rate + offset;
                        if (init_rate > 0) //проверка положительности рейта TS
                        {
                            ts_order = new TSOrder(user_id, amount, amount, init_rate, offset);
                            ShortTSs.Add(ts_order); //сортировка не нужна
                            Pusher.NewOrder((int)MessageTypes.NewAddTS, func_call_id, fc_source, position_type, ts_order); //сообщение о новом TS
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorIncorrectRate;
                    }
                    else return StatusCodes.ErrorInsufficientMarketVolume;
                }
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes RemoveSL(int user_id, long sl_id, long func_call_id, int fc_source, out bool pos_type, out Order order) //отменить стоп-лосс
        {
            //Pusher.ReplicateFC((int)FuncIds.RemoveSL, new string[] { user_id.ToString(), sl_id.ToString() }); //репликация вызова функции

            //инициализация
            pos_type = false;
            order = new Order();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то отменяем заявку
            {
                if (sl_id > 0) //проверка на положительность ID стоп-лосса
                {
                    int long_index = LongSLs.FindIndex(i => i.OrderId == sl_id);
                    if (long_index >= 0) //стоп-лосс найден в LongSLs
                    {
                        order = LongSLs[long_index];
                        if (order.UserId == user_id) //данный стоп-лосс принадлежит данному юзеру
                        {
                            pos_type = false;

                            LongSLs.RemoveAt(long_index);
                            Pusher.NewOrder((int)MessageTypes.NewRemoveSL, func_call_id, fc_source, pos_type, order); //сообщение о новой отмене SL
                            
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorCrossUserAccessDenied;
                    }
                    else
                    {
                        int short_index = ShortSLs.FindIndex(i => i.OrderId == sl_id);
                        if (short_index >= 0) //стоп-лосс найден в ShortSLs
                        {
                            order = ShortSLs[short_index];
                            if (order.UserId == user_id) //данный стоп-лосс принадлежит данному юзеру
                            {
                                pos_type = true;

                                ShortSLs.RemoveAt(short_index);
                                Pusher.NewOrder((int)MessageTypes.NewRemoveSL, func_call_id, fc_source, pos_type, order); //сообщение о новой отмене SL
                                
                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorCrossUserAccessDenied;
                        }
                        else return StatusCodes.ErrorOrderNotFound;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroId;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes RemoveTP(int user_id, long tp_id, long func_call_id, int fc_source, out bool pos_type, out Order order) //отменить тейк-профит
        {
            //Pusher.ReplicateFC((int)FuncIds.RemoveTP, new string[] { user_id.ToString(), tp_id.ToString() }); //репликация вызова функции

            //инициализация
            pos_type = false;
            order = new Order();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то отменяем заявку
            {
                if (tp_id > 0) //проверка на положительность ID тейк-профита
                {
                    int long_index = LongTPs.FindIndex(i => i.OrderId == tp_id);
                    if (long_index >= 0) //тейк-профит найден в LongTPs
                    {
                        order = LongTPs[long_index]; 
                        if (order.UserId == user_id) //данный тейк-профит принадлежит данному юзеру
                        {
                            pos_type = false;

                            LongTPs.RemoveAt(long_index);
                            Pusher.NewOrder((int)MessageTypes.NewRemoveTP, func_call_id, fc_source, pos_type, order); //сообщение о новой отмене TP
                            
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorCrossUserAccessDenied;
                    }
                    else
                    {
                        int short_index = ShortTPs.FindIndex(i => i.OrderId == tp_id);
                        if (short_index >= 0) //тейк-профит найден в ShortTPs
                        {
                            order = ShortTPs[short_index]; 
                            if (order.UserId == user_id) //данный тейк-профит принадлежит данному юзеру
                            {
                                pos_type = true;

                                ShortTPs.RemoveAt(short_index);
                                Pusher.NewOrder((int)MessageTypes.NewRemoveTP, func_call_id, fc_source, pos_type, order); //сообщение о новой отмене TP
                                
                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorCrossUserAccessDenied;
                        }
                        else return StatusCodes.ErrorOrderNotFound;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroId;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes RemoveTS(int user_id, long ts_id, long func_call_id, int fc_source, out bool pos_type, out TSOrder ts_order) //отменить трейлинг-стоп
        {
            //Pusher.ReplicateFC((int)FuncIds.RemoveTS, new string[] { user_id.ToString(), ts_id.ToString() }); //репликация вызова функции

            //инициализация
            pos_type = false;
            ts_order = new TSOrder();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то отменяем заявку
            {
                if (ts_id > 0) //проверка на положительность ID трейлинг-стопа
                {
                    int long_index = LongTSs.FindIndex(i => i.OrderId == ts_id);
                    if (long_index >= 0) //трейлинг-стоп найден в LongTSs
                    {
                        ts_order = LongTSs[long_index];
                        if (ts_order.UserId == user_id) //данный трейлинг-стоп принадлежит данному юзеру
                        {
                            pos_type = false;

                            LongTSs.RemoveAt(long_index);
                            Pusher.NewOrder((int)MessageTypes.NewRemoveTS, func_call_id, fc_source, pos_type, ts_order); //сообщение о новой отмене TS
                            
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorCrossUserAccessDenied;
                    }
                    else
                    {
                        int short_index = ShortTSs.FindIndex(i => i.OrderId == ts_id);
                        if (short_index >= 0) //трейлинг-стоп найден в ShortTSs
                        {
                            ts_order = ShortTSs[short_index];
                            if (ts_order.UserId == user_id) //данный трейлинг-стоп принадлежит данному юзеру
                            {
                                pos_type = true;

                                ShortTSs.RemoveAt(short_index);
                                Pusher.NewOrder((int)MessageTypes.NewRemoveTS, func_call_id, fc_source, pos_type, ts_order); //сообщение о новой отмене TS
                                
                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorCrossUserAccessDenied;
                        }
                        else return StatusCodes.ErrorOrderNotFound;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroId;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes CreateFixAccount(int user_id, out string sender_comp_id, out string password) //создать FIX-аккаунт
        {
            //Pusher.ReplicateFC((int)FuncIds.CreateFixAccount, new string[] { user_id.ToString() }); //репликация вызова функции

            //инициализация
            sender_comp_id = "";
            password = "";

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то генерируем FIX-аккаунт
            {
                //проверка на количество FIX-аккаунтов юзера
                int count = FixAccounts.Count(x => x.Value.UserId == user_id);
                if (count >= 10) return StatusCodes.ErrorFixAccountsLimitReached;
                
                sender_comp_id = "RCLT_" + user_id.ToString() + "_" + (count + 1).ToString();

                if (!FixAccounts.ContainsKey(sender_comp_id)) //если данного FIX-счёта нет, то открываем
                {
                    //генерация FIX-пароля
                    password = Guid.NewGuid().ToString("N").Substring(0, 16);

                    //создание FIX-счёта
                    FixAccounts.Add(sender_comp_id, new FixAccount(user_id, password));

                    return StatusCodes.Success;
                }
                else return StatusCodes.ErrorFixAccountAlreadyExists;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GenerateNewFixPassword(int user_id, string sender_comp_id, out string password) //сгенерировать новый FIX-пароль
        {
            //Pusher.ReplicateFC((int)FuncIds.GenerateNewFixPassword, new string[] { user_id.ToString(), sender_comp_id }); //репликация вызова функции

            //инициализация
            password = "";

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то изменяем FIX-пароль
            {
                FixAccount fix_acc; 
                if (FixAccounts.TryGetValue(sender_comp_id, out fix_acc)) //если данный FIX-счёт существует
                {
                    if (fix_acc.UserId == user_id) //данный FIX-счёт принадлежит данному юзеру
                    {
                        //генерация FIX-пароля
                        password = Guid.NewGuid().ToString("N").Substring(0, 16);

                        //изменение пароля и статуса FIX-счёта
                        fix_acc.Password = password;
                        fix_acc.Active = false;

                        return StatusCodes.Success;
                    }
                    else return StatusCodes.ErrorCrossUserAccessDenied;
                }
                else return StatusCodes.ErrorFixAccountNotFound;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GetFixAccounts(int user_id, out Dictionary<string, FixAccount> fix_accounts) //получить FIX-счета
        {
            //инициализация
            fix_accounts = new Dictionary<string, FixAccount>();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то возвращаем FIX-счета
            {
                foreach (KeyValuePair<string, FixAccount> acc in FixAccounts)
                {
                    if (acc.Value.UserId != user_id) continue;
                    else
                    {
                        fix_accounts.Add(acc.Key, acc.Value);
                    }
                }

                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes CancelFixAccount(int user_id, string sender_comp_id) //отменить FIX-счёт
        {
            //Pusher.ReplicateFC((int)FuncIds.CancelFixAccount, new string[] { user_id.ToString(), sender_comp_id }); //репликация вызова функции

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то отменяем FIX-счёт
            {
                FixAccount fix_acc;
                if (FixAccounts.TryGetValue(sender_comp_id, out fix_acc)) //если данный FIX-счёт существует
                {
                    if (fix_acc.UserId == user_id) //данный FIX-счёт принадлежит данному юзеру
                    {
                        //удаляем FIX-счёт
                        FixAccounts.Remove(sender_comp_id);
                        //удаляем очередь сообщений для данного FIX-счёта
                        Sys.fixman.RemoveFixAccountQueue(sender_comp_id);

                        return StatusCodes.Success;
                    }
                    else return StatusCodes.ErrorCrossUserAccessDenied;
                }
                else return StatusCodes.ErrorFixAccountNotFound;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }
        
        internal StatusCodes GenerateApiKey(int user_id, bool rights, out string key, out string secret) //сгенерировать API-ключ
        {
            //Pusher.ReplicateFC((int)FuncIds.GenerateApiKey, new string[] { user_id.ToString(), rights.ToInt32().ToString() }); //репликация вызова функции

            //инициализация
            key = "";
            secret = "";

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то генерируем API-ключ
            {
                //проверка на количество сгенерированных API-ключей юзера
                int count = ApiKeys.Count(x => x.Value.UserId == user_id);
                if (count >= 5) return StatusCodes.ErrorApiKeysLimitReached;

                //генерация API-ключа с проверкой на уникальность
                string new_key = "";
                bool key_not_unique = false;
                do 
                {
                    new_key = Guid.NewGuid().ToString().ToUpperInvariant();
                    key_not_unique = ApiKeys.ContainsKey(new_key) ? true : false;
                } while (key_not_unique);
                
                //запоминаем сгенерированный ключ
                key = new_key;

                //генерация secret
                secret = Guid.NewGuid().ToString("N").Substring(0, 16); //secret для проверки подлинности запросов

                //создание объекта API-ключа и сохранение его в памяти
                ApiKeys.Add(key, new ApiKey(user_id, Encoding.ASCII.GetBytes(secret), rights));
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GetApiKeys(int user_id, out Dictionary<string, ApiKey> api_keys) //получить API-ключи
        {
            //инициализация
            api_keys = new Dictionary<string, ApiKey>();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то возвращаем API-ключи
            {
                foreach (KeyValuePair<string, ApiKey> key in ApiKeys)
                {
                    if (key.Value.UserId != user_id) continue;
                    else
                    {
                        api_keys.Add(key.Key, key.Value);
                    }
                }

                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes CancelApiKey(int user_id, string key) //отменить API-ключ
        {
            //Pusher.ReplicateFC((int)FuncIds.CancelApiKey, new string[] { user_id.ToString(), key }); //репликация вызова функции

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то отменяем API-ключ
            {
                ApiKey api_key;
                if (ApiKeys.TryGetValue(key, out api_key)) //ключ найден в ApiKeys
                {
                    if (api_key.UserId == user_id) //данный ключ принадлежит данному юзеру
                    {
                        ApiKeys.Remove(key);
                        return StatusCodes.Success;
                    }
                    else return StatusCodes.ErrorCrossUserAccessDenied;
                }
                else return StatusCodes.ErrorApiKeyNotFound;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }
               
        internal StatusCodes GetAccountInfo(int user_id, out Account account) //получить данные аккаунта
        {
            //инициализация
            account = new Account();

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то пишем данные счёта
            {
                account = acc;

                return StatusCodes.Success; //возвращаем данные аккаунта
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GetOrderInfo(int user_id, long order_id, out bool order_kind, out Order order)
        {
            //инициализация
            order_kind = false;
            order = new Order();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то получаем заявку
            {
                if (order_id > 0) //проверка на положительность ID заявки
                {
                    order = ActiveBuyOrders.Find(i => i.OrderId == order_id);
                    if (order != null) //заявка найдена в ActiveBuyOrders
                    {
                        if (order.UserId == user_id) //данная заявка принадлежит данному юзеру
                        {
                            order_kind = false;

                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorCrossUserAccessDenied;
                    }
                    else
                    {
                        order = ActiveSellOrders.Find(i => i.OrderId == order_id);
                        if (order != null) //заявка найдена в ActiveSellOrders
                        {
                            if (order.UserId == user_id) //данная заявка принадлежит данному юзеру
                            {
                                order_kind = true;

                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorCrossUserAccessDenied;
                        }
                        else return StatusCodes.ErrorOrderNotFound;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroId;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GetSLInfo(int user_id, long sl_id, out bool pos_type, out Order order)
        {
            //инициализация
            pos_type = false;
            order = new Order();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то отменяем заявку
            {
                if (sl_id > 0) //проверка на положительность ID стоп-лосса
                {
                    order = LongSLs.Find(i => i.OrderId == sl_id);
                    if (order != null) //стоп-лосс найден в LongSLs
                    {
                        if (order.UserId == user_id) //данный стоп-лосс принадлежит данному юзеру
                        {
                            pos_type = false;

                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorCrossUserAccessDenied;
                    }
                    else
                    {
                        order = ShortSLs.Find(i => i.OrderId == sl_id);
                        if (order != null) //стоп-лосс найден в ShortSLs
                        {
                            if (order.UserId == user_id) //данный стоп-лосс принадлежит данному юзеру
                            {
                                pos_type = true;

                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorCrossUserAccessDenied;
                        }
                        else return StatusCodes.ErrorOrderNotFound;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroId;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GetTPInfo(int user_id, long tp_id, out bool pos_type, out Order order)
        {
            //инициализация
            pos_type = false;
            order = new Order();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то отменяем заявку
            {
                if (tp_id > 0) //проверка на положительность ID тейк-профита
                {
                    order = LongTPs.Find(i => i.OrderId == tp_id);
                    if (order != null) //тейк-профит найден в LongTPs
                    {
                        if (order.UserId == user_id) //данный тейк-профит принадлежит данному юзеру
                        {
                            pos_type = false;

                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorCrossUserAccessDenied;
                    }
                    else
                    {
                        order = ShortTPs.Find(i => i.OrderId == tp_id);
                        if (order != null) //тейк-профит найден в ShortTPs
                        {
                            if (order.UserId == user_id) //данный тейк-профит принадлежит данному юзеру
                            {
                                pos_type = true;

                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorCrossUserAccessDenied;
                        }
                        else return StatusCodes.ErrorOrderNotFound;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroId;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GetTSInfo(int user_id, long ts_id, out bool pos_type, out TSOrder ts_order)
        {
            //инициализация
            pos_type = false;
            ts_order = new TSOrder();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то отменяем заявку
            {
                if (ts_id > 0) //проверка на положительность ID трейлинг-стопа
                {
                    ts_order = LongTSs.Find(i => i.OrderId == ts_id);
                    if (ts_order != null) //трейлинг-стоп найден в LongTSs
                    {
                        if (ts_order.UserId == user_id) //данный трейлинг-стоп принадлежит данному юзеру
                        {
                            pos_type = false;

                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorCrossUserAccessDenied;
                    }
                    else
                    {
                        ts_order = ShortTSs.Find(i => i.OrderId == ts_id);
                        if (ts_order != null) //трейлинг-стоп найден в ShortTSs
                        {
                            if (ts_order.UserId == user_id) //данный трейлинг-стоп принадлежит данному юзеру
                            {
                                pos_type = true;

                                return StatusCodes.Success;
                            }
                            else return StatusCodes.ErrorCrossUserAccessDenied;
                        }
                        else return StatusCodes.ErrorOrderNotFound;
                    }
                }
                else return StatusCodes.ErrorNegativeOrZeroId;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GetOpenOrders(int user_id, out List<Order> open_buy, out List<Order> open_sell) //получить открытые заявки
        {
            //инициализация
            open_buy = new List<Order>();
            open_sell = new List<Order>();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то пишем активные заявки
            {
                open_buy = ActiveBuyOrders.FindAll(i => i.UserId == user_id); //активные заявки на покупку
                open_sell = ActiveSellOrders.FindAll(i => i.UserId == user_id); //активные заявки на продажу
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes GetOpenConditionalOrders(int user_id, out List<Order> long_sls, out List<Order> short_sls, out List<Order> long_tps, out List<Order> short_tps, out List<TSOrder> long_tss, out List<TSOrder> short_tss) //получить открытые SL/TP/TS
        {
            //инициализация
            long_sls = new List<Order>();
            short_sls = new List<Order>();
            long_tps = new List<Order>();
            short_tps = new List<Order>();
            long_tss = new List<TSOrder>();
            short_tss = new List<TSOrder>();

            if (Accounts.ContainsKey(user_id)) //если счёт существует, то пишем активные SL/TP/TS
            {
                //стоп-лоссы юзера
                long_sls = LongSLs.FindAll(i => i.UserId == user_id);
                short_sls = ShortSLs.FindAll(i => i.UserId == user_id);

                //тейк-профиты юзера
                long_tps = LongTPs.FindAll(i => i.UserId == user_id);
                short_tps = ShortTPs.FindAll(i => i.UserId == user_id);

                //трейлинг-стопы юзера
                long_tss = LongTSs.FindAll(i => i.UserId == user_id);
                short_tss = ShortTSs.FindAll(i => i.UserId == user_id);

                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        internal StatusCodes SetAccountFee(int user_id, decimal fee_in_perc, long func_call_id) //задать индивидуальную комиссию
        {
            //Pusher.ReplicateFC((int)FuncIds.SetAccountFee, new string[] { user_id.ToString(), fee_in_perc.ToString() }); //репликация вызова функции

            Account acc;
            if (Accounts.TryGetValue(user_id, out acc)) //если счёт существует, то изменяем индивидуальную комиссию
            {
                if (fee_in_perc >= 0 && fee_in_perc <= 100) //проверка на корректность процентного значения
                {
                    acc.Fee = fee_in_perc / 100m;
                    Pusher.NewAccountFee(func_call_id, user_id, fee_in_perc, DateTime.Now); //сообщение о новой комиссии

                    return StatusCodes.Success;
                }
                else return StatusCodes.ErrorIncorrectPercValue;
            }
            else return StatusCodes.ErrorAccountNotFound;
        }

        #endregion

        #region GLOBAL FUNCTIONS

        internal StatusCodes GetTicker(out decimal bid_price, out decimal ask_price) //получить тикер
        {
            //инициализация
            bid_price = 0m;
            ask_price = 0m;

            if (ActiveBuyOrders.Count > 0) bid_price = ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate; //bid price
            if (ActiveSellOrders.Count > 0) ask_price = ActiveSellOrders[ActiveSellOrders.Count - 1].Rate; //ask price

            return StatusCodes.Success;
        }

        internal StatusCodes GetDepth(int limit, out List<OrderBuf> bids, out List<OrderBuf> asks, out decimal bids_vol, out decimal asks_vol, out int bids_num, out int asks_num) //получить стаканы
        {
            //инициализация
            bids = new List<OrderBuf>();
            asks = new List<OrderBuf>();
            bids_vol = 0m;
            asks_vol = 0m;
            bids_num = 0;
            asks_num = 0;

            if (limit > 0) //проверка на положительность лимита
            {
                //получаем заявки на покупку
                if (ActiveBuyOrders.Count > 0)
                {
                    decimal accumulated_amount = ActiveBuyOrders[ActiveBuyOrders.Count - 1].ActualAmount;
                    decimal last_rate = ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate;
                    for (int i = ActiveBuyOrders.Count - 2; i >= 0; i--)
                    {
                        if (ActiveBuyOrders[i].Rate == last_rate)
                        {
                            accumulated_amount += ActiveBuyOrders[i].ActualAmount;
                        }
                        else
                        {
                            if (bids.Count == limit - 1) break;

                            //добавляем в буфер accumulated_amount и last_rate до их изменения                    
                            bids.Add(new OrderBuf(accumulated_amount, last_rate));

                            Order buy_ord = ActiveBuyOrders[i];
                            accumulated_amount = buy_ord.ActualAmount;
                            last_rate = buy_ord.Rate;
                        }
                    }
                    bids.Add(new OrderBuf(accumulated_amount, last_rate));
                }

                //получаем заявки на продажу
                if (ActiveSellOrders.Count > 0)
                {
                    decimal accumulated_amount = ActiveSellOrders[ActiveSellOrders.Count - 1].ActualAmount;
                    decimal last_rate = ActiveSellOrders[ActiveSellOrders.Count - 1].Rate;
                    for (int i = ActiveSellOrders.Count - 2; i >= 0; i--)
                    {
                        if (ActiveSellOrders[i].Rate == last_rate)
                        {
                            accumulated_amount += ActiveSellOrders[i].ActualAmount;
                        }
                        else
                        {
                            if (asks.Count == limit - 1) break;

                            //добавляем в буфер accumulated_amount и last_rate до их изменения                    
                            asks.Add(new OrderBuf(accumulated_amount, last_rate));

                            Order sell_ord = ActiveSellOrders[i];
                            accumulated_amount = sell_ord.ActualAmount;
                            last_rate = sell_ord.Rate;
                        }
                    }
                    asks.Add(new OrderBuf(accumulated_amount, last_rate));
                }

                bids_vol = ActiveBuyOrders.Sum(item => item.ActualAmount * item.Rate); //bids' volume (in currency2)
                asks_vol = ActiveSellOrders.Sum(item => item.ActualAmount); //asks' volume (in currency1) 
                bids_num = ActiveBuyOrders.Count; //bids' num                       
                asks_num = ActiveSellOrders.Count; //asks' num

                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorNegativeOrZeroLimit;
        }
        
        internal StatusCodes GetMarginParameters(out decimal max_leverage_val, out decimal mc_level_in_perc, out decimal fl_level_in_perc) //получить текущие значения маржинальных параметров
        {
            max_leverage_val = max_leverage;
            mc_level_in_perc = mc_level * 100m;
            fl_level_in_perc = fl_level * 100m;

            return StatusCodes.Success;
        }

        internal StatusCodes SetMaxLeverage(decimal max_leverage_val) //задать значение максимального плеча
        {
            //Pusher.ReplicateFC((int)FuncIds.SetMaxLeverage, new string[] { max_leverage_val.ToString() }); //репликация вызова функции

            if (max_leverage_val > 0) //проверка на положительность значения плеча
            {
                max_leverage = max_leverage_val;

                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorNegativeOrZeroLeverage;
        }

        internal StatusCodes SetMCLevel(decimal mc_level_in_perc) //задать процентное значение уровня Margin Call
        {
            //Pusher.ReplicateFC((int)FuncIds.SetMCLevel, new string[] { mc_level_in_perc.ToString() }); //репликация вызова функции

            if (mc_level_in_perc >= 0 && mc_level_in_perc <= 100) //проверка на корректность процентного значения
            {
                mc_level = mc_level_in_perc / 100m;

                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorIncorrectPercValue;
        }

        internal StatusCodes SetFLLevel(decimal fl_level_in_perc) //задать процентное значение уровня Forced Liquidation
        {
            //Pusher.ReplicateFC((int)FuncIds.SetFLLevel, new string[] { fl_level_in_perc.ToString() }); //репликация вызова функции

            if (fl_level_in_perc >= 0 && fl_level_in_perc <= 100) //проверка на корректность процентного значения
            {
                fl_level = fl_level_in_perc / 100m;

                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorIncorrectPercValue;
        }

        #endregion

        #endregion

        #region HTTP API USER FUNCTIONS

        internal StatusCodes GetAccountInfo(string key, string signature, long nonce, out Account account) //получить данные аккаунта (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                return GetAccountInfo(user_id, out account);
            }
            else //ошибка авторизации
            {
                //инициализация
                account = new Account();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes GetOrderInfo(string key, string signature, long nonce, long order_id, out bool order_kind, out Order order) //получить данные заявки (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                return GetOrderInfo(user_id, order_id, out order_kind, out order);
            }
            else //ошибка авторизации
            {
                //инициализация
                order_kind = false;
                order = new Order();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes GetSLInfo(string key, string signature, long nonce, long sl_id, out bool pos_type, out Order order) //получить данные SL (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                return GetSLInfo(user_id, sl_id, out pos_type, out order);
            }
            else //ошибка авторизации
            {
                //инициализация
                pos_type = false;
                order = new Order();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes GetTPInfo(string key, string signature, long nonce, long tp_id, out bool pos_type, out Order order) //получить данные TP (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                return GetTPInfo(user_id, tp_id, out pos_type, out order);
            }
            else //ошибка авторизации
            {
                //инициализация
                pos_type = false;
                order = new Order();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes GetTSInfo(string key, string signature, long nonce, long ts_id, out bool pos_type, out TSOrder ts_order) //получить данные TS (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                return GetTSInfo(user_id, ts_id, out pos_type, out ts_order);
            }
            else //ошибка авторизации
            {
                //инициализация
                pos_type = false;
                ts_order = new TSOrder();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes GetOpenOrders(string key, string signature, long nonce, out List<Order> open_buy, out List<Order> open_sell) //получить открытые заявки (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                return GetOpenOrders(user_id, out open_buy, out open_sell);
            }
            else //ошибка авторизации
            {
                //инициализация
                open_buy = new List<Order>();
                open_sell = new List<Order>();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes GetOpenConditionalOrders(string key, string signature, long nonce, out List<Order> long_sls, out List<Order> short_sls, out List<Order> long_tps, out List<Order> short_tps, out List<TSOrder> long_tss, out List<TSOrder> short_tss) //получить открытые SL/TP/TS (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                return GetOpenConditionalOrders(user_id, out long_sls, out short_sls, out long_tps, out short_tps, out long_tss, out short_tss);
            }
            else //ошибка авторизации
            {
                //инициализация
                long_sls = new List<Order>();
                short_sls = new List<Order>();
                long_tps = new List<Order>();
                short_tps = new List<Order>();
                long_tss = new List<TSOrder>();
                short_tss = new List<TSOrder>();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes PlaceLimit(string key, string signature, long nonce, bool order_kind, decimal amount, decimal rate, long func_call_id, out Order order) //подать лимитную заявку (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return PlaceLimit(user_id, order_kind, amount, rate, func_call_id, (int)FCSources.HttpApi, out order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else //ошибка авторизации
            {
                //инициализация
                order = new Order();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes PlaceMarket(string key, string signature, long nonce, bool order_kind, decimal amount, long func_call_id, out Order order) //подать рыночную заявку (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return PlaceMarket(user_id, order_kind, amount, func_call_id, (int)FCSources.HttpApi, out order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else //ошибка авторизации
            {
                //инициализация
                order = new Order();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes PlaceInstant(string key, string signature, long nonce, bool order_kind, decimal total, long func_call_id, out Order order) //подать рыночную заявку (на входе валюта 2) (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return PlaceInstant(user_id, order_kind, total, func_call_id, (int)FCSources.HttpApi, out order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else //ошибка авторизации
            {
                //инициализация
                order = new Order();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes CancelOrder(string key, string signature, long nonce, long order_id, long func_call_id, out bool order_kind, out Order order) //отменить заявку (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return CancelOrder(user_id, order_id, func_call_id, (int)FCSources.HttpApi, out order_kind, out order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    order_kind = false;
                    order = new Order();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else
            {
                //инициализация
                order_kind = false;
                order = new Order();

                return auth_status;
            }
        }

        internal StatusCodes AddSL(string key, string signature, long nonce, bool position_type, decimal amount, decimal rate, long func_call_id, out Order order) //добавить стоп-лосс (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return AddSL(user_id, position_type, amount, rate, func_call_id, (int)FCSources.HttpApi, out order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else //ошибка авторизации
            {
                //инициализация
                order = new Order();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes AddTP(string key, string signature, long nonce, bool position_type, decimal amount, decimal rate, long func_call_id, out Order order) //добавить тейк-профит (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return AddTP(user_id, position_type, amount, rate, func_call_id, (int)FCSources.HttpApi, out order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    order = new Order();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else //ошибка авторизации
            {
                //инициализация
                order = new Order();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes AddTS(string key, string signature, long nonce, bool position_type, decimal amount, decimal offset, long func_call_id, out TSOrder ts_order) //добавить трейлинг-стоп (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return AddTS(user_id, position_type, amount, offset, func_call_id, (int)FCSources.HttpApi, out ts_order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    ts_order = new TSOrder();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else //ошибка авторизации
            {
                //инициализация
                ts_order = new TSOrder();

                return auth_status; //возвращаем ошибку авторизации
            }
        }

        internal StatusCodes RemoveSL(string key, string signature, long nonce, long sl_id, long func_call_id, out bool pos_type, out Order order) //отменить стоп-лосс (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return RemoveSL(user_id, sl_id, func_call_id, (int)FCSources.HttpApi, out pos_type, out order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    pos_type = false;
                    order = new Order();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else
            {
                //инициализация
                pos_type = false;
                order = new Order();

                return auth_status;
            }
        }

        internal StatusCodes RemoveTP(string key, string signature, long nonce, long tp_id, long func_call_id, out bool pos_type, out Order order) //отменить тейк-профит (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return RemoveTP(user_id, tp_id, func_call_id, (int)FCSources.HttpApi, out pos_type, out order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    pos_type = false;
                    order = new Order();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else
            {
                //инициализация
                pos_type = false;
                order = new Order();

                return auth_status;
            }
        }

        internal StatusCodes RemoveTS(string key, string signature, long nonce, long ts_id, long func_call_id, out bool pos_type, out TSOrder ts_order) //отменить трейлинг-стоп (HTTP API)
        {
            int user_id;
            bool rights;

            StatusCodes auth_status = Authorize(key, signature, nonce, out user_id, out rights); //аутентификация API-запроса
            if (auth_status == StatusCodes.Success) //авторизация прошла успешно
            {
                if (rights) //проверяем права API-ключа
                {
                    return RemoveTS(user_id, ts_id, func_call_id, (int)FCSources.HttpApi, out pos_type, out ts_order);
                }
                else //недостаточно прав API-ключа
                {
                    //инициализация
                    pos_type = false;
                    ts_order = new TSOrder();

                    return StatusCodes.ErrorApiKeyNotPrivileged;
                }
            }
            else
            {
                //инициализация
                pos_type = false;
                ts_order = new TSOrder();

                return auth_status;
            }
        }
               
        #endregion
        
        #endregion

        #region SERVICE CORE FUNCTIONS

        #region MATCHING

        private void Match() //выполняет мэтчинг текущих активных заявок, возвращает массив совершившихся сделок
        {
            //если хотя бы одна из коллекций пустая, то выход из мэтчинга
            if ((Accounts.Count == 0) || (ActiveBuyOrders.Count == 0) || (ActiveSellOrders.Count == 0))
            {
                if (UpdTicker()) Pusher.NewTicker(bid_buf, ask_buf, DateTime.Now); //сообщение о новом тикере
                if (UpdActiveBuyTop()) Pusher.NewActiveBuyTop(act_buy_buf, DateTime.Now); //сообщение о новом топе стакана на покупку
                if (UpdActiveSellTop()) Pusher.NewActiveSellTop(act_sell_buf, DateTime.Now); //сообщение о новом топе стакана на продажу
                return;
            }

            //sw.Start();
         
            //итеративный алгоритм метчинга по топовым заявкам
            while (ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate >= ActiveSellOrders[ActiveSellOrders.Count - 1].Rate)
            {
                //сохранение указателей на заявки и счета
                Order buy_ord = ActiveBuyOrders[ActiveBuyOrders.Count - 1];
                Order sell_ord = ActiveSellOrders[ActiveSellOrders.Count - 1];
                Account buyer = Accounts[buy_ord.UserId];
                Account seller = Accounts[sell_ord.UserId];

                //поиск наиболее ранней заявки для определения initiator_kind, trade_rate (и комиссий)
                bool initiator_kind;
                decimal trade_rate = 0;
                if (buy_ord.OrderId < sell_ord.OrderId)
                {
                    initiator_kind = false; //buy
                    trade_rate = buy_ord.Rate;
                    //дифференциация комиссий maker-taker тут
                }
                else
                {
                    initiator_kind = true; //sell
                    trade_rate = sell_ord.Rate;
                    //дифференциация комиссий maker-taker тут
                }

                //три варианта в зависимости от объёма каждой из 2-х выполняемых заявок
                if (buy_ord.ActualAmount > sell_ord.ActualAmount) //1-ый вариант - объём buy-заявки больше
                {
                    //добавляем объект Trade в коллекцию
                    Trade trade = new Trade(buy_ord.OrderId, sell_ord.OrderId, buy_ord.UserId, sell_ord.UserId, initiator_kind, sell_ord.ActualAmount, trade_rate, sell_ord.ActualAmount * buyer.Fee, sell_ord.ActualAmount * trade_rate * seller.Fee);
                    Pusher.NewTrade(trade); //сообщение о новой сделке

                    //начисляем продавцу сумму минус комиссия
                    seller.BlockedFunds1 -= sell_ord.ActualAmount;
                    seller.AvailableFunds2 += sell_ord.ActualAmount * trade_rate * (1m - seller.Fee);
                    Pusher.NewBalance(sell_ord.UserId, seller, DateTime.Now); //сообщение о новом балансе

                    //начисляем покупателю сумму минус комиссия плюс разницу
                    buyer.BlockedFunds2 -= sell_ord.ActualAmount * buy_ord.Rate;
                    buyer.AvailableFunds1 += sell_ord.ActualAmount * (1m - buyer.Fee);
                    buyer.AvailableFunds2 += sell_ord.ActualAmount * (buy_ord.Rate - trade_rate);
                    Pusher.NewBalance(buy_ord.UserId, buyer, DateTime.Now); //сообщение о новом балансе

                    //buy-заявка становится partially filled => уменьшается её ActualAmount
                    buy_ord.ActualAmount -= sell_ord.ActualAmount;
                    Pusher.NewOrderStatus(buy_ord.OrderId, buy_ord.UserId, (int)OrdExecStatus.PartiallyFilled, DateTime.Now); //сообщение о новом статусе заявки

                    //sell-заявка становится filled => её ActualAmount становится нулевым
                    sell_ord.ActualAmount = 0;
                    Pusher.NewOrderStatus(sell_ord.OrderId, sell_ord.UserId, (int)OrdExecStatus.Filled, DateTime.Now); //сообщение о новом статусе заявки

                    //FIX multicast
                    //FixMessager.NewMarketDataIncrementalRefresh(trade);

                    //FIX-сообщения о новых сделках
                    if (buy_ord.FCSource == (int)FCSources.FixApi) FixMessager.NewExecutionReport(buy_ord.ExternalData, false, buy_ord, trade);
                    if (sell_ord.FCSource == (int)FCSources.FixApi) FixMessager.NewExecutionReport(sell_ord.ExternalData, true, sell_ord, trade);

                    //т.к. объём buy-заявки больше, sell-заявка удаляется из списка активных заявок
                    ActiveSellOrders.RemoveAt(ActiveSellOrders.Count - 1);
                }
                else if (buy_ord.ActualAmount < sell_ord.ActualAmount) //2-ой вариант - объём sell-заявки больше
                {
                    //добавляем объект Trade в коллекцию
                    Trade trade = new Trade(buy_ord.OrderId, sell_ord.OrderId, buy_ord.UserId, sell_ord.UserId, initiator_kind, buy_ord.ActualAmount, trade_rate, buy_ord.ActualAmount * buyer.Fee, buy_ord.ActualAmount * trade_rate * seller.Fee);
                    Pusher.NewTrade(trade); //сообщение о новой сделке

                    //начисляем продавцу сумму минус комиссия
                    seller.BlockedFunds1 -= buy_ord.ActualAmount;
                    seller.AvailableFunds2 += buy_ord.ActualAmount * trade_rate * (1m - seller.Fee);
                    Pusher.NewBalance(sell_ord.UserId, seller, DateTime.Now); //сообщение о новом балансе

                    //начисляем покупателю сумму минус комиссия плюс разницу
                    buyer.BlockedFunds2 -= buy_ord.ActualAmount * buy_ord.Rate;
                    buyer.AvailableFunds1 += buy_ord.ActualAmount * (1m - buyer.Fee);
                    buyer.AvailableFunds2 += buy_ord.ActualAmount * (buy_ord.Rate - trade_rate);
                    Pusher.NewBalance(buy_ord.UserId, buyer, DateTime.Now); //сообщение о новом балансе

                    //sell-заявка становится partially filled => уменьшается её ActualAmount
                    sell_ord.ActualAmount -= buy_ord.ActualAmount;
                    Pusher.NewOrderStatus(sell_ord.OrderId, sell_ord.UserId, (int)OrdExecStatus.PartiallyFilled, DateTime.Now); //сообщение о новом статусе заявки

                    //buy-заявка становится filled => её ActualAmount становится нулевым
                    buy_ord.ActualAmount = 0;
                    Pusher.NewOrderStatus(buy_ord.OrderId, buy_ord.UserId, (int)OrdExecStatus.Filled, DateTime.Now); //сообщение о новом статусе заявки

                    //FIX multicast
                    //FixMessager.NewMarketDataIncrementalRefresh(trade);

                    //FIX-сообщения о новых сделках
                    if (buy_ord.FCSource == (int)FCSources.FixApi) FixMessager.NewExecutionReport(buy_ord.ExternalData, false, buy_ord, trade);
                    if (sell_ord.FCSource == (int)FCSources.FixApi) FixMessager.NewExecutionReport(sell_ord.ExternalData, true, sell_ord, trade);

                    //т.к. объём sell-заявки больше, buy-заявка удаляется из списка активных заявок
                    ActiveBuyOrders.RemoveAt(ActiveBuyOrders.Count - 1);
                }
                else if (buy_ord.ActualAmount == sell_ord.ActualAmount) //3-ий вариант - объёмы заявок равны
                {
                    //добавляем объект Trade в коллекцию
                    Trade trade = new Trade(buy_ord.OrderId, sell_ord.OrderId, buy_ord.UserId, sell_ord.UserId, initiator_kind, buy_ord.ActualAmount, trade_rate, sell_ord.ActualAmount * buyer.Fee, sell_ord.ActualAmount * trade_rate * seller.Fee);
                    Pusher.NewTrade(trade); //сообщение о новой сделке

                    //начисляем продавцу сумму минус комиссия
                    seller.BlockedFunds1 -= buy_ord.ActualAmount;
                    seller.AvailableFunds2 += buy_ord.ActualAmount * trade_rate * (1m - seller.Fee);
                    Pusher.NewBalance(sell_ord.UserId, seller, DateTime.Now); //сообщение о новом балансе

                    //начисляем покупателю сумму минус комиссия плюс разницу
                    buyer.BlockedFunds2 -= buy_ord.ActualAmount * buy_ord.Rate;
                    buyer.AvailableFunds1 += buy_ord.ActualAmount * (1m - buyer.Fee);
                    buyer.AvailableFunds2 += buy_ord.ActualAmount * (buy_ord.Rate - trade_rate);
                    Pusher.NewBalance(buy_ord.UserId, buyer, DateTime.Now); //сообщение о новом балансе

                    //buy-заявка становится filled => её ActualAmount становится нулевым
                    buy_ord.ActualAmount = 0;
                    Pusher.NewOrderStatus(buy_ord.OrderId, buy_ord.UserId, (int)OrdExecStatus.Filled, DateTime.Now); //сообщение о новом статусе заявки

                    //sell-заявка становится filled => её ActualAmount становится нулевым
                    sell_ord.ActualAmount = 0;
                    Pusher.NewOrderStatus(sell_ord.OrderId, sell_ord.UserId, (int)OrdExecStatus.Filled, DateTime.Now); //сообщение о новом статусе заявки

                    //FIX multicast
                    //FixMessager.NewMarketDataIncrementalRefresh(trade);

                    //FIX-сообщения о новых сделках
                    if (buy_ord.FCSource == (int)FCSources.FixApi) FixMessager.NewExecutionReport(buy_ord.ExternalData, false, buy_ord, trade);
                    if (sell_ord.FCSource == (int)FCSources.FixApi) FixMessager.NewExecutionReport(sell_ord.ExternalData, true, sell_ord, trade);

                    //т.к. объёмы заявок равны, обе заявки удаляются из списка активных заявок
                    ActiveBuyOrders.RemoveAt(ActiveBuyOrders.Count - 1);
                    ActiveSellOrders.RemoveAt(ActiveSellOrders.Count - 1);
                }

                //если все заявки в стакане (стаканах) были удалены - выходим из цикла
                if ((ActiveBuyOrders.Count == 0) || (ActiveSellOrders.Count == 0)) break;
            }

            //sw.Stop();
            //Console.WriteLine("Matching time: " + sw.ElapsedMilliseconds + " ms");
            
            if (UpdTicker()) Pusher.NewTicker(bid_buf, ask_buf, DateTime.Now); //сообщение о новом тикере
            if (UpdActiveBuyTop()) Pusher.NewActiveBuyTop(act_buy_buf, DateTime.Now); //сообщение о новом топе стакана на покупку
            if (UpdActiveSellTop()) Pusher.NewActiveSellTop(act_sell_buf, DateTime.Now); //сообщение о новом топе стакана на продажу
        }

        private bool UpdTicker()
        {
            //обновление тикера
            bool _upd = false;
            if (ActiveBuyOrders.Count > 0)
            {
                Order buy_top_ord = ActiveBuyOrders[ActiveBuyOrders.Count - 1];
                if (bid_buf != buy_top_ord.Rate)
                {
                    bid_buf = buy_top_ord.Rate;
                    _upd = true;
                }
            }
            if (ActiveSellOrders.Count > 0)
            {
                Order sell_top_ord = ActiveSellOrders[ActiveSellOrders.Count - 1];
                if (ask_buf != sell_top_ord.Rate)
                {
                    ask_buf = sell_top_ord.Rate;
                    _upd = true;
                }
            }
            return _upd;
        }

        private bool UpdActiveBuyTop()
        {
            //обновление топа ActiveBuyOrders
            bool _upd = false;

            List<OrderBuf> ActBuyCopy = new List<OrderBuf>(30);
            if (ActiveBuyOrders.Count > 0)
            {
                decimal accumulated_amount = ActiveBuyOrders[ActiveBuyOrders.Count - 1].ActualAmount;
                decimal last_rate = ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate;
                for (int i = ActiveBuyOrders.Count - 2; i >= 0; i--)
                {
                    if (ActiveBuyOrders[i].Rate == last_rate)
                    {
                        accumulated_amount += ActiveBuyOrders[i].ActualAmount;
                    }
                    else
                    {
                        if (ActBuyCopy.Count == act_buy_buf_max_size - 1) break;

                        //добавляем в буфер accumulated_amount и last_rate до их изменения                    
                        ActBuyCopy.Add(new OrderBuf(accumulated_amount, last_rate));

                        Order buy_ord = ActiveBuyOrders[i];
                        accumulated_amount = buy_ord.ActualAmount;
                        last_rate = buy_ord.Rate;
                    }
                }
                ActBuyCopy.Add(new OrderBuf(accumulated_amount, last_rate));
            }

            if (!EqualOrderBooks(act_buy_buf, ActBuyCopy))
            {
                act_buy_buf = ActBuyCopy;
                _upd = true;
            }
            return _upd;
        }

        private bool UpdActiveSellTop()
        {
            //обновление топа ActiveSellOrders
            bool _upd = false;

            List<OrderBuf> ActSellCopy = new List<OrderBuf>(30);
            if (ActiveSellOrders.Count > 0)
            {
                decimal accumulated_amount = ActiveSellOrders[ActiveSellOrders.Count - 1].ActualAmount;
                decimal last_rate = ActiveSellOrders[ActiveSellOrders.Count - 1].Rate;
                for (int i = ActiveSellOrders.Count - 2; i >= 0; i--)
                {
                    if (ActiveSellOrders[i].Rate == last_rate)
                    {
                        accumulated_amount += ActiveSellOrders[i].ActualAmount;
                    }
                    else
                    {
                        if (ActSellCopy.Count == act_sell_buf_max_size - 1) break;

                        //добавляем в буфер accumulated_amount и last_rate до их изменения                    
                        ActSellCopy.Add(new OrderBuf(accumulated_amount, last_rate));

                        Order sell_ord = ActiveSellOrders[i];
                        accumulated_amount = sell_ord.ActualAmount;
                        last_rate = sell_ord.Rate;
                    }
                }
                ActSellCopy.Add(new OrderBuf(accumulated_amount, last_rate));
            }

            if (!EqualOrderBooks(act_sell_buf, ActSellCopy))
            {
                act_sell_buf = ActSellCopy;
                _upd = true;
            }
            return _upd;
        }

        private bool EqualOrderBooks(List<OrderBuf> book1, List<OrderBuf> book2)
        {
            if (book1.Count != book2.Count) return false;
            else
            {
                for (int i = 0; i < book1.Count; i++)
                {
                    if (!book1[i].Equals(book2[i])) return false;
                }
                return true;
            }
        }

        #endregion

        #region PERIODIC EXECUTION

        internal void ManageMargin() //рассчитывает ML, выполняет MC / FL в случае необходимости
        {
            //Pusher.ReplicateFC((int)FuncIds.ManageMargin, new string[0]); //репликация вызова функции

            //проверяем условия для каждого юзера
            foreach (KeyValuePair<int, Account> account in Accounts)
            {
                if (account.Value.AvailableFunds1 < 0) //проверка шортов
                {
                    //калькуляция рыночного курса на продажу (шорт будет выкупаться в случае FL) 
                    decimal short_market_rate = 0m;
                    decimal short_accumulated_amount = 0m;
                    decimal borrowed_sum = account.Value.AvailableFunds1 * (-1m);
                    for (int j = ActiveSellOrders.Count - 1; j >= 0; j--)
                    {
                        short_accumulated_amount += ActiveSellOrders[j].ActualAmount;
                        if (short_accumulated_amount >= borrowed_sum) //если объём накопленных заявок на продажу покрывает объём заявки на покупку
                        {
                            short_market_rate = ActiveSellOrders[j].Rate;
                            break;
                        }
                    }

                    if (short_market_rate == 0 || account.Value.AvailableFunds2 == 0) continue;

                    decimal amount = borrowed_sum / (1m - account.Value.Fee);

                    //рассчитываем Margin Level
                    decimal margin_level = 1m - (amount * short_market_rate) / account.Value.AvailableFunds2;
                    if (account.Value.MarginLevel != margin_level)
                    {
                        account.Value.MarginLevel = margin_level;
                        Pusher.NewMarginLevel(account.Key, margin_level * 100m, DateTime.Now); //сообщение о новом уровне маржи
                    }

                    if (account.Value.AvailableFunds2 * (1m - mc_level) < amount * short_market_rate) //условие Margin Call
                    {
                        if (!account.Value.MarginCall)
                        {                            
                            account.Value.MarginCall = true;
                            Pusher.NewMarginCall(account.Key, DateTime.Now); //сообщение о новом Margin Call
                        }
                    }
                    else if (account.Value.MarginCall) account.Value.MarginCall = false; //сброс флага Margin Call

                    if (account.Value.AvailableFunds2 * (1m - fl_level) < amount * short_market_rate) //условие Forced Liquidation
                    {
                        //выкупаем шорт юзера с помощью рыночной заявки на покупку                        
                        account.Value.AvailableFunds2 -= amount * short_market_rate; //снимаем средства с доступных средств
                        account.Value.BlockedFunds2 += amount * short_market_rate; //блокируем средства в заявке на покупку
                        Pusher.NewBalance(account.Key, account.Value, DateTime.Now); //сообщение о новом балансе
                        Order new_buy_order = new Order(account.Key, amount, amount, short_market_rate);
                        BSInsertion.AddBuyOrder(ref ActiveBuyOrders, new_buy_order);
                        Pusher.NewOrder((int)MessageTypes.NewForcedLiquidation, false, new_buy_order); //сообщение о новой FL-заявке
                        //FixMessager.NewMarketDataIncrementalRefresh(false, new_buy_order); //FIX multicast
                        Match(); //мэтчинг FL-заявки

                    }
                }
                else if (account.Value.AvailableFunds2 < 0) //проверка лонгов
                {
                    //калькуляция рыночного курса на покупку (лонг будет распродаваться в случае FL) 
                    decimal long_market_rate = 0m;
                    decimal long_accumulated_total = 0m;
                    decimal borrowed_sum = account.Value.AvailableFunds2 * (-1m);
                    for (int j = ActiveBuyOrders.Count - 1; j >= 0; j--)
                    {
                        long_accumulated_total += ActiveBuyOrders[j].ActualAmount * ActiveBuyOrders[j].Rate;
                        if (long_accumulated_total >= borrowed_sum) //если объём накопленных заявок на продажу покрывает объём заявки на продажу
                        {
                            long_market_rate = ActiveBuyOrders[j].Rate;
                            break;
                        }
                    }

                    if (long_market_rate == 0 || account.Value.AvailableFunds1 == 0) continue;

                    //рассчитываем Margin Level
                    decimal margin_level = 1m - borrowed_sum / (account.Value.AvailableFunds1 * long_market_rate);
                    if (account.Value.MarginLevel != margin_level)
                    {
                        account.Value.MarginLevel = margin_level;
                        Pusher.NewMarginLevel(account.Key, margin_level * 100m, DateTime.Now); //сообщение о новом уровне маржи
                    }

                    if (account.Value.AvailableFunds1 * long_market_rate * (1m - mc_level) < borrowed_sum) //условие Margin Call
                    {
                        if (!account.Value.MarginCall)
                        {
                            account.Value.MarginCall = true;
                            Pusher.NewMarginCall(account.Key, DateTime.Now); //сообщение о новом Margin Call
                        }
                    }
                    else if (account.Value.MarginCall) account.Value.MarginCall = false; //сброс флага Margin Call

                    if (account.Value.AvailableFunds1 * long_market_rate * (1m - fl_level) < borrowed_sum) //условие Forced Liquidation
                    {
                        //распродаём лонг юзера с помощью рыночной заявки на продажу
                        decimal amount = account.Value.AvailableFunds1;
                        account.Value.AvailableFunds1 -= amount; //снимаем средства с доступных средств
                        account.Value.BlockedFunds1 += amount; //блокируем средства в заявке на покупку
                        Pusher.NewBalance(account.Key, account.Value, DateTime.Now); //сообщение о новом балансе
                        Order new_sell_order = new Order(account.Key, amount, amount, long_market_rate);
                        BSInsertion.AddSellOrder(ref ActiveSellOrders, new_sell_order);
                        Pusher.NewOrder((int)MessageTypes.NewForcedLiquidation, true, new_sell_order); //сообщение о новой FL-заявке
                        //FixMessager.NewMarketDataIncrementalRefresh(true, new_sell_order); //FIX multicast
                        Match(); //мэтчинг FL-заявки
                    }
                }
                else //юзер не использует заёмные средства
                {
                    if (account.Value.MarginCall) account.Value.MarginCall = false; //если юзер закрыл позицию, то сбрасываем флаг Margin Call
                    if (account.Value.MarginLevel != 1m)
                    {
                        account.Value.MarginLevel = 1m; //если юзер закрыл позицию, сбрасываем Margin Level
                        Pusher.NewMarginLevel(account.Key, 100m, DateTime.Now); //сообщение о новом уровне маржи
                    }
                }
            }
        }

        internal void ManageSLs()
        {
            //Pusher.ReplicateFC((int)FuncIds.ManageSLs, new string[0]); //репликация вызова функции

            //проверяем условия SL для лонгов
            for (int i = LongSLs.Count - 1; i >= 0; i--)
            {
                if (ActiveBuyOrders.Count > 0)
                {
                    Order long_sl = LongSLs[i];
                    if (ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate <= long_sl.Rate) //сравнение с рыночным курсом на покупку (стоп-лосс будет на продажу)
                    {
                        //создаём рыночную заявку на продажу по рынку
                        //калькуляция rate для немедленного исполнения по рынку
                        decimal market_rate = 0m;
                        decimal accumulated_amount = 0m;
                        for (int j = ActiveBuyOrders.Count - 1; j >= 0; j--)
                        {
                            accumulated_amount += ActiveBuyOrders[j].ActualAmount;
                            if (accumulated_amount >= long_sl.ActualAmount) //если объём накопленных заявок на покупку покрывает объём заявки на продажу
                            {
                                market_rate = ActiveBuyOrders[j].Rate;
                                break;
                            }
                        }

                        if (market_rate == 0) continue;

                        Account acc = Accounts[long_sl.UserId];
                        if (acc.AvailableFunds1 >= long_sl.ActualAmount) //проверка на платежеспособность по currency1
                        {
                            acc.AvailableFunds1 -= long_sl.ActualAmount; //снимаем средства с доступных средств
                            acc.BlockedFunds1 += long_sl.ActualAmount; //блокируем средства в заявке на продажу
                            Pusher.NewBalance(long_sl.UserId, acc, DateTime.Now); //сообщение о новом балансе
                            Order new_sell_order = new Order(long_sl.UserId, long_sl.ActualAmount, long_sl.ActualAmount, market_rate, long_sl.FCSource, long_sl.ExternalData);
                            LongSLs.RemoveAt(i); //удаляем SL из памяти
                            BSInsertion.AddSellOrder(ref ActiveSellOrders, new_sell_order);
                            Pusher.NewOrder((int)MessageTypes.NewExecSL, true, new_sell_order); //сообщение о срабатывании SL
                            //FixMessager.NewMarketDataIncrementalRefresh(true, new_sell_order); //FIX multicast
                            Match(); //мэтчинг SL-заявки
                        }
                        else
                        {
                            LongSLs.RemoveAt(i); //удаляем SL из памяти 
                        }
                    }
                    else break;
                }
            }

            //проверяем условия SL для шортов
            for (int i = ShortSLs.Count - 1; i >= 0; i--)
            {
                if (ActiveSellOrders.Count > 0)
                {
                    Order short_sl = ShortSLs[i];
                    if (ActiveSellOrders[ActiveSellOrders.Count - 1].Rate >= short_sl.Rate) //сравнение с рыночным курсом на продажу (стоп-лосс будет на покупку)
                    {
                        //создаём рыночную заявку на покупку по рынку
                        //калькуляция rate для немедленного исполнения по рынку
                        decimal market_rate = 0m;
                        decimal accumulated_amount = 0m;
                        for (int j = ActiveSellOrders.Count - 1; j >= 0; j--)
                        {
                            accumulated_amount += ActiveSellOrders[j].ActualAmount;
                            if (accumulated_amount >= short_sl.ActualAmount) //если объём накопленных заявок на продажу покрывает объём заявки на покупку
                            {
                                market_rate = ActiveSellOrders[j].Rate;
                                break;
                            }
                        }

                        if (market_rate == 0) continue;

                        Account acc = Accounts[short_sl.UserId];
                        decimal amount = short_sl.ActualAmount / (1m - acc.Fee);
                        if (acc.AvailableFunds2 >= amount * market_rate) //проверка на платежеспособность по currency2
                        {
                            acc.AvailableFunds2 -= amount * market_rate; //снимаем средства с доступных средств
                            acc.BlockedFunds2 += amount * market_rate; //блокируем средства в заявке на продажу
                            Pusher.NewBalance(short_sl.UserId, acc, DateTime.Now); //сообщение о новом балансе
                            Order new_buy_order = new Order(short_sl.UserId, amount, amount, market_rate, short_sl.FCSource, short_sl.ExternalData);
                            ShortSLs.RemoveAt(i); //удаляем SL из памяти
                            BSInsertion.AddBuyOrder(ref ActiveBuyOrders, new_buy_order);
                            Pusher.NewOrder((int)MessageTypes.NewExecSL, false, new_buy_order); //сообщение о срабатывании SL
                            //FixMessager.NewMarketDataIncrementalRefresh(false, new_buy_order); //FIX multicast
                            Match(); //мэтчинг SL-заявки
                        }
                        else
                        {
                            ShortSLs.RemoveAt(i); //удаляем SL из памяти 
                        }
                    }
                    else break;
                }
            }
        }

        internal void ManageTPs()
        {
            //Pusher.ReplicateFC((int)FuncIds.ManageTPs, new string[0]); //репликация вызова функции

            //проверяем условия TP для лонгов
            for (int i = LongTPs.Count - 1; i >= 0; i--)
            {
                if (ActiveBuyOrders.Count > 0)
                {
                    Order long_tp = LongTPs[i];
                    if (ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate >= long_tp.Rate) //сравнение с рыночным курсом на покупку (тейк-профит будет на продажу)
                    {
                        //создаём рыночную заявку на продажу по рынку
                        //калькуляция rate для немедленного исполнения по рынку
                        decimal market_rate = 0m;
                        decimal accumulated_amount = 0m;
                        for (int j = ActiveBuyOrders.Count - 1; j >= 0; j--)
                        {
                            accumulated_amount += ActiveBuyOrders[j].ActualAmount;
                            if (accumulated_amount >= long_tp.ActualAmount) //если объём накопленных заявок на покупку покрывает объём заявки на продажу
                            {
                                market_rate = ActiveBuyOrders[j].Rate;
                                break;
                            }
                        }

                        if (market_rate == 0) continue;

                        Account acc = Accounts[long_tp.UserId];
                        if (acc.AvailableFunds1 >= long_tp.ActualAmount) //проверка на платежеспособность по currency1
                        {
                            acc.AvailableFunds1 -= long_tp.ActualAmount; //снимаем средства с доступных средств
                            acc.BlockedFunds1 += long_tp.ActualAmount; //блокируем средства в заявке на продажу
                            Pusher.NewBalance(long_tp.UserId, acc, DateTime.Now); //сообщение о новом балансе
                            Order new_sell_order = new Order(long_tp.UserId, long_tp.ActualAmount, long_tp.ActualAmount, market_rate);
                            LongTPs.RemoveAt(i); //удаляем TP из памяти
                            BSInsertion.AddSellOrder(ref ActiveSellOrders, new_sell_order);
                            Pusher.NewOrder((int)MessageTypes.NewExecTP, true, new_sell_order); //сообщение о срабатывании TP
                            //FixMessager.NewMarketDataIncrementalRefresh(true, new_sell_order); //FIX multicast
                            Match(); //мэтчинг TP-заявки
                        }
                        else
                        {
                            LongTPs.RemoveAt(i); //удаляем TP из памяти 
                        }
                    }
                    else break;
                }
            }

            //проверяем условия TP для шортов
            for (int i = ShortTPs.Count - 1; i >= 0; i--)
            {
                if (ActiveSellOrders.Count > 0)
                {
                    Order short_tp = ShortTPs[i];
                    if (ActiveSellOrders[ActiveSellOrders.Count - 1].Rate <= short_tp.Rate) //сравнение с рыночным курсом на продажу (тейк-профит будет на покупку)
                    {
                        //создаём рыночную заявку на покупку по рынку
                        //калькуляция rate для немедленного исполнения по рынку
                        decimal market_rate = 0m;
                        decimal accumulated_amount = 0m;
                        for (int j = ActiveSellOrders.Count - 1; j >= 0; j--)
                        {
                            accumulated_amount += ActiveSellOrders[j].ActualAmount;
                            if (accumulated_amount >= short_tp.ActualAmount) //если объём накопленных заявок на покупку покрывает объём заявки на продажу
                            {
                                market_rate = ActiveSellOrders[j].Rate;
                                break;
                            }
                        }

                        if (market_rate == 0) continue;

                        Account acc = Accounts[short_tp.UserId];
                        decimal amount = short_tp.ActualAmount / (1m - acc.Fee);
                        if (acc.AvailableFunds2 >= amount * market_rate) //проверка на платежеспособность по currency2
                        {
                            acc.AvailableFunds2 -= amount * market_rate; //снимаем средства с доступных средств
                            acc.BlockedFunds2 += amount * market_rate; //блокируем средства в заявке на продажу
                            Pusher.NewBalance(short_tp.UserId, acc, DateTime.Now); //сообщение о новом балансе
                            Order new_buy_order = new Order(short_tp.UserId, amount, amount, market_rate);
                            ShortTPs.RemoveAt(i); //удаляем TP из памяти
                            BSInsertion.AddBuyOrder(ref ActiveBuyOrders, new_buy_order);
                            Pusher.NewOrder((int)MessageTypes.NewExecTP, false, new_buy_order); //сообщение о срабатывании TP
                            //FixMessager.NewMarketDataIncrementalRefresh(false, new_buy_order); //FIX multicast
                            Match(); //мэтчинг TP-заявки
                        }
                        else
                        {
                            ShortTPs.RemoveAt(i); //удаляем TP из памяти 
                        }
                    }
                    else break;
                }
            }
        }

        internal void ManageTSs()
        {
            //Pusher.ReplicateFC((int)FuncIds.ManageTSs, new string[0]); //репликация вызова функции

            //проверяем условия TS для лонгов
            for (int i = LongTSs.Count - 1; i >= 0; i--)
            {
                if (ActiveBuyOrders.Count > 0)
                {
                    TSOrder long_ts = LongTSs[i];
                    decimal cur_ts_rate = ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate - long_ts.Offset;
                    if (cur_ts_rate > long_ts.Rate) //произошло повышение рыночной цены на покупку
                    {
                        //подтягиваем наверх цену TS
                        long_ts.Rate = cur_ts_rate;                        
                    }
                    else if (cur_ts_rate == long_ts.Rate) break; //цена не изменилась
                    else //цена упала => проверяем условие на срабатывание TS
                    {
                        if (ActiveBuyOrders[ActiveBuyOrders.Count - 1].Rate <= long_ts.Rate) //сравнение с рыночным курсом на покупку (заявка будет на продажу)
                        {
                            //создаём рыночную заявку на продажу по рынку
                            //калькуляция rate для немедленного исполнения по рынку
                            decimal market_rate = 0m;
                            decimal accumulated_amount = 0m;
                            for (int j = ActiveBuyOrders.Count - 1; j >= 0; j--)
                            {
                                accumulated_amount += ActiveBuyOrders[j].ActualAmount;
                                if (accumulated_amount >= long_ts.ActualAmount) //если объём накопленных заявок на покупку покрывает объём заявки на продажу
                                {
                                    market_rate = ActiveBuyOrders[j].Rate;
                                    break;
                                }
                            }

                            if (market_rate == 0) continue;

                            Account acc = Accounts[long_ts.UserId];
                            if (acc.AvailableFunds1 >= long_ts.ActualAmount) //проверка на платежеспособность по currency1
                            {
                                acc.AvailableFunds1 -= long_ts.ActualAmount; //снимаем средства с доступных средств
                                acc.BlockedFunds1 += long_ts.ActualAmount; //блокируем средства в заявке на продажу
                                Pusher.NewBalance(long_ts.UserId, acc, DateTime.Now); //сообщение о новом балансе
                                Order new_sell_order = new Order(long_ts.UserId, long_ts.ActualAmount, long_ts.ActualAmount, market_rate);
                                LongTSs.RemoveAt(i); //удаляем TS из памяти
                                BSInsertion.AddSellOrder(ref ActiveSellOrders, new_sell_order);
                                Pusher.NewOrder((int)MessageTypes.NewExecTS, true, new_sell_order); //сообщение о срабатывании TS
                                //FixMessager.NewMarketDataIncrementalRefresh(true, new_sell_order); //FIX multicast
                                Match(); //мэтчинг TS-заявки
                            }
                            else
                            {
                                LongTSs.RemoveAt(i); //удаляем TS из памяти 
                            }
                        }
                    }
                }
            }

            //проверяем условия TS для шортов
            for (int i = ShortTSs.Count - 1; i >= 0; i--)
            {
                if (ActiveSellOrders.Count > 0)
                {
                    TSOrder short_ts = ShortTSs[i];
                    decimal cur_ts_rate = ActiveSellOrders[ActiveSellOrders.Count - 1].Rate + short_ts.Offset;
                    if (cur_ts_rate < short_ts.Rate) //произошло понижение рыночной цены на продажу
                    {
                        //подтягиваем вниз цену TS
                        short_ts.Rate = cur_ts_rate;
                    }
                    else if (cur_ts_rate == short_ts.Rate) break; //цена не изменилась
                    else //цена выросла => проверяем условие на срабатывание TS
                    {
                        if (ActiveSellOrders[ActiveSellOrders.Count - 1].Rate >= short_ts.Rate) //сравнение с рыночным курсом на покупку (заявка будет на покупку)
                        {
                            //создаём рыночную заявку на покупку по рынку
                            //калькуляция rate для немедленного исполнения по рынку
                            decimal market_rate = 0m;
                            decimal accumulated_amount = 0m;
                            for (int j = ActiveSellOrders.Count - 1; j >= 0; j--)
                            {
                                accumulated_amount += ActiveSellOrders[j].ActualAmount;
                                if (accumulated_amount >= short_ts.ActualAmount) //если объём накопленных заявок на продажу покрывает объём заявки на покупку
                                {
                                    market_rate = ActiveSellOrders[j].Rate;
                                    break;
                                }
                            }

                            if (market_rate == 0) continue;

                            Account acc = Accounts[short_ts.UserId];
                            decimal amount = short_ts.ActualAmount / (1m - acc.Fee);
                            if (acc.AvailableFunds2 >= amount * market_rate) //проверка на платежеспособность по currency2
                            {
                                acc.AvailableFunds2 -= amount * market_rate; //снимаем средства с доступных средств
                                acc.BlockedFunds2 += amount * market_rate; //блокируем средства в заявке на продажу
                                Pusher.NewBalance(short_ts.UserId, acc, DateTime.Now); //сообщение о новом балансе
                                Order new_buy_order = new Order(short_ts.UserId, amount, amount, market_rate);
                                ShortTSs.RemoveAt(i); //удаляем TS из памяти
                                BSInsertion.AddBuyOrder(ref ActiveBuyOrders, new_buy_order);
                                Pusher.NewOrder((int)MessageTypes.NewExecTS, false, new_buy_order); //сообщение о срабатывании TS
                                //FixMessager.NewMarketDataIncrementalRefresh(false, new_buy_order); //FIX multicast
                                Match(); //мэтчинг TS-заявки
                            }
                            else
                            {
                                ShortTSs.RemoveAt(i); //удаляем TS из памяти 
                            }
                        }
                    }
                }
            }
        }
                
        #endregion

        #region API KEYS MANAGEMENT

        private StatusCodes Authorize(string key, string signature, long nonce, out int user_id, out bool rights)
        {
            //Pusher.ReplicateFC((int)FuncIds.Authorize, new string[] { key, signature, nonce.ToString() }); //репликация вызова функции

            //инициализация
            user_id = 0;
            rights = false;

            ApiKey api_key;
            if (ApiKeys.TryGetValue(key, out api_key)) //если ключ найден
            {
                if (api_key.LastSignature != signature) //если подпись не равна предыдущей подписи
                {
                    if (api_key.LastNonce < nonce) //если полученный nonce больше записанного в памяти 
                    {
                        //вычисляем ожидаемую подпись и сравниваем с полученной
                        string expected_sign;
                        using (HMACSHA512 hmac = new HMACSHA512(api_key.Secret))
                        {
                            byte[] hash = hmac.ComputeHash(Encoding.ASCII.GetBytes(nonce.ToString() + api_key.UserId.ToString() + key)); //nonce + user_id + api_key 
                            expected_sign = hash.Hexadecimal().ToUpperInvariant();
                        }

                        if (expected_sign == signature) //в этом случае обновляем LastSignature и LastNonce
                        {
                            api_key.LastSignature = signature;
                            api_key.LastNonce = nonce;
                            user_id = api_key.UserId;
                            rights = api_key.Rights;
                            return StatusCodes.Success;
                        }
                        else return StatusCodes.ErrorIncorrectSignature;
                    }
                    else return StatusCodes.ErrorNonceLessThanExpected;
                }
                else return StatusCodes.ErrorSignatureDuplicate;
            }
            else return StatusCodes.ErrorApiKeyNotFound;
        }

        private void RemoveUserApiKeys(int user_id)
        {
            foreach (KeyValuePair<string, ApiKey> key in ApiKeys)
            {
                if (key.Value.UserId != user_id) continue;
                else
                {
                    ApiKeys.Remove(key.Key);
                }
            }
        }

        #endregion

        #region FIX ACCOUNTS MANAGEMENT

        internal string[] GetSenderCompIds() //получение массива sender_comp_id из коллекции FixAccounts
        {
            int count = FixAccounts.Count;
            string[] sender_comp_ids = FixAccounts.Keys.ToArray();

            return sender_comp_ids;
        }

        internal string LookupFixPassword(string sender_comp_id)
        {
            FixAccount acc;
            if (FixAccounts.TryGetValue(sender_comp_id, out acc))
            {
                return acc.Password;
            }
            else return "";
        }

        internal int LookupFixUserId(string sender_comp_id)
        {
            FixAccount acc;
            if (FixAccounts.TryGetValue(sender_comp_id, out acc))
            {
                return acc.UserId;
            }
            else return 0;
        }

        internal void MarkFixAccountsActive() //отметка, что текущие FIX-аккаунты вступили в силу
        {
            //Pusher.ReplicateFC((int)FuncIds.MarkFixAccountsActive, new string[0]); //репликация вызова функции

            foreach (FixAccount acc in FixAccounts.Values)
            {
                acc.Active = true;
            }
        }

        private void RemoveUserFixAccounts(int user_id)
        {
            foreach (KeyValuePair<string, FixAccount> acc in FixAccounts)
            {
                if (acc.Value.UserId != user_id) continue;
                else
                {
                    FixAccounts.Remove(acc.Key);
                }
            }
        }

        #endregion
        
        #endregion
        
    }
}
