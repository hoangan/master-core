﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using QuickFix;
using QuickFix.Fields;

namespace RocketCore
{
    class FixAcceptor : MessageCracker, IApplication
    {
        private FixSender sender = new FixSender();

        public void OnMessage(QuickFix.FIX44.NewOrderSingle ord, SessionID sessionID) //New Order - Single
        {
            if (!ord.IsSetClOrdID() || !ord.IsSetSymbol() || !ord.IsSetSide() || !ord.IsSetTransactTime() || !ord.IsSetOrdType() || !ord.IsSetPrice() || !ord.IsSetCashOrderQty())
            {
                //ошибка - не заданы поля заявки
                SendFieldsNotSetExecReport(sessionID);
                return;
            }

            if (Flags.market_closed)
            {
                //ошибка - market closed
                SendErrorExecReport(StatusCodes.ErrorMarketClosed, ord, sessionID);
                return;
            }

            if (ord.ClOrdID.getValue().Contains('&')) //запрещаем ClOrdID с '&', т.к. это разделитель external_data
            {
                //ошибка - некорректный ClOrdID
                SendErrorExecReport(StatusCodes.ErrorFixInvalidClOrdID, ord, sessionID);
                return;
            }
            
            bool order_kind = (ord.Side.getValue() == Side.BUY) ? false : true;
            decimal amount = ord.CashOrderQty.getValue();
            decimal rate = ord.Price.getValue(); 
            
            FuncCall call;
            Order order;
            string external_data =  sessionID.TargetCompID + '&' + ord.ClOrdID.getValue();

            switch (ord.OrdType.getValue()) //форк по типу заявки
            {
                case OrdType.LIMIT:
                    {
                        call = new FuncCall();
                        call.Action = () =>
                        {
                            int user_id = Sys.core.LookupFixUserId(sessionID.TargetCompID);
                            StatusCodes status = Sys.core.PlaceLimit(user_id, order_kind, amount, rate, call.FuncCallId, (int)FCSources.FixApi, out order, external_data);
                            if (status != StatusCodes.Success) FixMessager.NewExecutionReport(sessionID.TargetCompID, status, call.FuncCallId, ord, order);
                        };
                        Console.WriteLine("FIX: to queue core.PlaceLimit(...)");
                        break;
                    }
                case OrdType.MARKET:
                    {
                        call = new FuncCall();
                        call.Action = () =>
                        {
                            int user_id = Sys.core.LookupFixUserId(sessionID.TargetCompID);
                            StatusCodes status = Sys.core.PlaceMarket(user_id, order_kind, amount, call.FuncCallId, (int)FCSources.FixApi, out order, external_data);
                            if (status != StatusCodes.Success) FixMessager.NewExecutionReport(sessionID.TargetCompID, status, call.FuncCallId, ord, order);
                        };
                        Console.WriteLine("FIX: to queue core.PlaceMarket(...)");
                        break;
                    }
                case OrdType.STOP:
                    {
                        call = new FuncCall();
                        call.Action = () =>
                        {
                            int user_id = Sys.core.LookupFixUserId(sessionID.TargetCompID);
                            StatusCodes status = Sys.core.AddSL(user_id, order_kind, amount, rate, call.FuncCallId, (int)FCSources.FixApi, out order, external_data);
                            if (status != StatusCodes.Success) FixMessager.NewExecutionReport(sessionID.TargetCompID, status, call.FuncCallId, ord, order);
                        };
                        Console.WriteLine("FIX: to queue core.AddSL(...)");
                        break;
                    }
                default:
                    {
                        //ошибка - указан неизвестный тип заявки
                        SendErrorExecReport(StatusCodes.ErrorFixUnknownOrderType, ord, sessionID);
                        return;
                    }
            }

            //ставим в очередь вызов функции
            Queues.stdf_queue.Enqueue(call);

            Console.WriteLine("FIX: queued NewOrderSingle");
        }

        public void OnMessage(QuickFix.FIX44.OrderStatusRequest stareq, SessionID sessionID) //Order Status Request
        {
            if (!stareq.IsSetClOrdID() || !stareq.IsSetOrderID())
            {
                //ошибка - не заданы поля запроса статуса
                SendFieldsNotSetExecReport(sessionID);
                return;
            }

            long order_id;
            if (!long.TryParse(stareq.OrderID.getValue(), out order_id))
            {
                //ошибка - невалидный ID заявки
                SendErrorExecReport(StatusCodes.ErrorFixInvalidOrderId, stareq, sessionID);
                return;
            }
            
            bool order_kind;
            Order order;

            FuncCall call = new FuncCall();
            call.Action = () =>
            {
                int user_id = Sys.core.LookupFixUserId(sessionID.TargetCompID);
                StatusCodes status = Sys.core.GetOrderInfo(user_id, order_id, out order_kind, out order);
                if (status == StatusCodes.ErrorOrderNotFound) status = Sys.core.GetSLInfo(user_id, order_id, out order_kind, out order);
                FixMessager.NewExecutionReport(sessionID.TargetCompID, status, stareq, order_kind, order);
            };

            //ставим в очередь вызов функции
            Queues.stdf_queue.Enqueue(call);

            Console.WriteLine("Queued FIX OrderStatusRequest");
        }

        public void OnMessage(QuickFix.FIX44.OrderCancelRequest canreq, SessionID sessionID) //Order Cancel Request
        {
            if (!canreq.IsSetClOrdID() || !canreq.IsSetOrderID() || !canreq.IsSetTransactTime())
            {
                //ошибка - не заданы поля запроса отмены заявки
                SendFieldsNotSetOrderCancelReject(sessionID);
                return;
            }

            if (Flags.market_closed)
            {
                //ошибка - market closed
                SendOrderCancelReject(StatusCodes.ErrorMarketClosed, canreq, sessionID);
                return;
            }

            long order_id;
            if (!long.TryParse(canreq.OrderID.getValue(), out order_id))
            {
                //ошибка - невалидный ID заявки
                SendOrderCancelReject(StatusCodes.ErrorFixInvalidOrderId, canreq, sessionID);
                return;
            }

            bool order_kind;
            Order order;

            FuncCall call = new FuncCall();
            call.Action = () =>
            {
                int user_id = Sys.core.LookupFixUserId(sessionID.TargetCompID);
                StatusCodes status = Sys.core.CancelOrder(user_id, order_id, call.FuncCallId, (int)FCSources.FixApi, out order_kind, out order);
                if (status == StatusCodes.ErrorOrderNotFound) status = Sys.core.RemoveSL(user_id, order_id, call.FuncCallId, (int)FCSources.FixApi, out order_kind, out order);
                if (status == StatusCodes.Success) FixMessager.NewExecutionReport(sessionID.TargetCompID, call.FuncCallId, canreq, order_kind, order);
                else FixMessager.NewOrderCancelReject(sessionID.TargetCompID, status, call.FuncCallId, canreq);
            };

            //ставим в очередь вызов функции
            Queues.stdf_queue.Enqueue(call);

            Console.WriteLine("Queued FIX OrderCancelRequest");
        }

        private void SendFieldsNotSetExecReport(SessionID s) //отклонение регистрации заявки в случае отсутствия обязательных полей
        {
            QuickFix.FIX44.ExecutionReport exec_report = new QuickFix.FIX44.ExecutionReport(
                new OrderID("0"),
                new ExecID("0"),
                new ExecType(ExecType.REJECTED),
                new OrdStatus(OrdStatus.REJECTED),
                new Symbol("BTC_USD"),
                new Side(Side.BUY),
                new LeavesQty(0m),
                new CumQty(0m),
                new AvgPx(0m)
                );

            exec_report.Set(new Text(StatusCodes.ErrorFixFieldsNotSet.ToString()));
            exec_report.Set(new TransactTime(DateTime.Now));

            SendMessage(exec_report, s);
        }
               
        private void SendErrorExecReport(StatusCodes err_code, QuickFix.FIX44.NewOrderSingle ord, SessionID s) //отклонение регистрации заявки в случае ошибочных данных в полях
        {
            QuickFix.FIX44.ExecutionReport exec_report = new QuickFix.FIX44.ExecutionReport(
                new OrderID("0"),
                new ExecID("0"),
                new ExecType(ExecType.REJECTED),
                new OrdStatus(OrdStatus.REJECTED),
                new Symbol("BTC_USD"),
                ord.Side,
                new LeavesQty(0m),
                new CumQty(0m),
                new AvgPx(0m)
                );
            
            exec_report.Set(new Text(err_code.ToString()));
            exec_report.Set(ord.Price);
            exec_report.Set(ord.ClOrdID);
            exec_report.Set(ord.CashOrderQty);
            exec_report.Set(new TransactTime(DateTime.Now));

            SendMessage(exec_report, s);
        }

        private void SendErrorExecReport(StatusCodes err_code, QuickFix.FIX44.OrderStatusRequest stareq, SessionID s) //отклонение получения статуса заявки в случае ошибочных данных в полях
        {
            QuickFix.FIX44.ExecutionReport exec_report = new QuickFix.FIX44.ExecutionReport(
                stareq.OrderID,
                new ExecID("0"),
                new ExecType(ExecType.REJECTED),
                new OrdStatus(OrdStatus.REJECTED),
                new Symbol("BTC_USD"),
                new Side(Side.BUY),
                new LeavesQty(0m),
                new CumQty(0m),
                new AvgPx(0m)
                );

            exec_report.Set(new Text(err_code.ToString()));

            SendMessage(exec_report, s);
        }

        private void SendFieldsNotSetOrderCancelReject(SessionID s) //отклонение отмены заявки в случае отсутствия обязательных полей
        {
            QuickFix.FIX44.OrderCancelReject ocr = new QuickFix.FIX44.OrderCancelReject(
                new OrderID("0"),
                new ClOrdID("0"),
                new OrigClOrdID("0"),
                new OrdStatus(OrdStatus.REJECTED),
                new CxlRejResponseTo(CxlRejResponseTo.ORDER_CANCEL_REQUEST)
                );

            ocr.Set(new Text(StatusCodes.ErrorFixFieldsNotSet.ToString()));

            SendMessage(ocr, s);
        }

        private void SendOrderCancelReject(StatusCodes err_code, QuickFix.FIX44.OrderCancelRequest canreq, SessionID s) //отклонение отмены заявки в случае ошибочных данных в полях
        {
            QuickFix.FIX44.OrderCancelReject ocr = new QuickFix.FIX44.OrderCancelReject(
                canreq.OrderID,
                canreq.ClOrdID,
                new OrigClOrdID("0"),
                new OrdStatus(OrdStatus.REJECTED),
                new CxlRejResponseTo(CxlRejResponseTo.ORDER_CANCEL_REQUEST)
                );

            ocr.Set(new Text(err_code.ToString()));

            SendMessage(ocr, s);
        }

        private void SendMessage(Message m, SessionID s) //отправление любого FIX-сообщения
        {
            try
            {
                Session.SendToTarget(m, s);
                Console.WriteLine("FIX MESSAGE SENT");
            }
            catch (SessionNotFound ex)
            {
                Console.WriteLine("==fix session not found exception!==");
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #region FIX CALLBACKS

        public void FromApp(Message msg, SessionID sessionID)
        {
            if (Flags.backup_restore_in_proc) return; //проверка на резервирование или восстановление снэпшота

            Console.WriteLine("FIX: received " + msg.ToString());
            try
            {
                Crack(msg, sessionID);
            }
            catch (Exception ex)
            {
                Console.WriteLine("==Cracker exception==");
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.StackTrace);
            }
        }
        public void OnCreate(SessionID sessionID) 
        {
            //при рестарте попытка добавления выгребающей очереди для данного FIX-логина (SenderCompId)
            Queues.fix_dict.TryAdd(sessionID.TargetCompID, new ConcurrentQueue<Message>());
        }
        public void OnLogout(SessionID sessionID) { }
        public void OnLogon(SessionID sessionID) 
        {            
            Session s = Session.LookupSession(sessionID);
            if (s != null) sender.SpawnFixSenderThread(s);
        }
        public void FromAdmin(Message msg, SessionID sessionID)
        {
            //проверка пароля в FIX Logon
            if (msg.Header.GetString(35) == QuickFix.FIX44.Logon.MsgType)
            {
                Console.WriteLine("FIX: reading Logon message...");

                string sender_comp_id = sessionID.TargetCompID;

                if (!msg.IsSetField(554))
                {
                    //ошибка - не указан пароль
                    msg.Clear();
                    return;
                }

                string password = msg.GetString(554);

                Console.WriteLine("FIX: message read:");
                Console.WriteLine(msg.ToString());
                Console.Write("FIX: SenderCompID: ");
                Console.WriteLine(sender_comp_id);
                Console.Write("FIX: password provided: ");
                Console.WriteLine(password);

                string stored_pass = null;
                FuncCall call = new FuncCall();
                call.Action = () =>
                {
                    stored_pass = Sys.core.LookupFixPassword(sender_comp_id);
                };
                //ставим в очередь вызов функции
                Queues.stdf_queue.Enqueue(call);

                Console.WriteLine("FIX: queued password lookup, sender_comp_id: " + sender_comp_id + "");

                //таймаут 5 секунд
                bool timeout = false;
                DateTime lookup_time = DateTime.Now;
                while (stored_pass == null)
                {
                    Thread.Sleep(100);
                    if (lookup_time.AddSeconds(5) <= DateTime.Now)
                    {
                        timeout = true;
                        break;
                    }
                }

                if (!timeout) 
                {
                    if (stored_pass.Length == 0 || stored_pass != password) msg.Clear(); //не даём доступ, если а) FIX-аккаунт не найден б) неверная пара логин-пароль
                }
                else msg.Clear(); //произошёл таймаут получения FIX-пароля
            }
        }
        public void ToAdmin(Message msg, SessionID sessionID) { }
        public void ToApp(Message msg, SessionID sessionID) { }

        #endregion

    }
}
