﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class MarginLevelMsg : IJsonSerializable
    {
        private int user_id;
        private decimal ml_in_perc;
        private DateTime dt_made;

        internal MarginLevelMsg(int user_id, decimal ml_in_perc, DateTime dt_made) //конструктор сообщения
        {
            this.user_id = user_id;
            this.ml_in_perc = ml_in_perc;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewMarginLevel, user_id, ml_in_perc, dt_made);
        }
    }
}
